﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FIORest.Migrations.SqliteMigrations
{
    public partial class TonsOfUpsertChanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BuyingAds_LocalMarketModels_LocalMarketModelId",
                table: "BuyingAds");

            migrationBuilder.DropForeignKey(
                name: "FK_ContractConditions_ContractModels_ContractModelId",
                table: "ContractConditions");

            migrationBuilder.DropForeignKey(
                name: "FK_ContractDependencies_ContractConditions_ContractConditionId",
                table: "ContractDependencies");

            migrationBuilder.DropForeignKey(
                name: "FK_CXOSTradeOrders_CXOSTradeOrderModels_CXOSTradeOrdersModelId",
                table: "CXOSTradeOrders");

            migrationBuilder.DropForeignKey(
                name: "FK_CXOSTrades_CXOSTradeOrders_CXOSTradeOrderId",
                table: "CXOSTrades");

            migrationBuilder.DropForeignKey(
                name: "FK_CXPCDataEntries_CXPCData_CXPCDataId",
                table: "CXPCDataEntries");

            migrationBuilder.DropForeignKey(
                name: "FK_FLIGHTSDestinationLines_FLIGHTSFlightSegments_FLIGHTSFlightSegmentId",
                table: "FLIGHTSDestinationLines");

            migrationBuilder.DropForeignKey(
                name: "FK_FLIGHTSOriginLines_FLIGHTSFlightSegments_FLIGHTSFlightSegmentId",
                table: "FLIGHTSOriginLines");

            migrationBuilder.DropForeignKey(
                name: "FK_InfrastructureModelReports_InfrastructureModels_InfrastructureModelId",
                table: "InfrastructureModelReports");

            migrationBuilder.DropForeignKey(
                name: "FK_InfrastructureProjectContributions_InfrastructureInfos_InfrastructureInfoId",
                table: "InfrastructureProjectContributions");

            migrationBuilder.DropForeignKey(
                name: "FK_InfrastructureProjectUpgradeCosts_InfrastructureInfos_InfrastructureInfoId",
                table: "InfrastructureProjectUpgradeCosts");

            migrationBuilder.DropForeignKey(
                name: "FK_InfrastructureProjectUpkeeps_InfrastructureInfos_InfrastructureInfoId",
                table: "InfrastructureProjectUpkeeps");

            migrationBuilder.DropForeignKey(
                name: "FK_JumpCacheRoutes_JumpCacheModels_JumpCacheModelId",
                table: "JumpCacheRoutes");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductionLineInputs_ProductionLineOrders_ProductionLineOrderId",
                table: "ProductionLineInputs");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductionLineOrders_ProductionLines_ProductionLineId",
                table: "ProductionLineOrders");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductionLineOutputs_ProductionLineOrders_ProductionLineOrderId",
                table: "ProductionLineOutputs");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductionLines_PRODLinesModels_PRODLinesModelId",
                table: "ProductionLines");

            migrationBuilder.DropForeignKey(
                name: "FK_SellingAds_LocalMarketModels_LocalMarketModelId",
                table: "SellingAds");

            migrationBuilder.DropForeignKey(
                name: "FK_ShippingAds_LocalMarketModels_LocalMarketModelId",
                table: "ShippingAds");

            migrationBuilder.DropForeignKey(
                name: "FK_StorageItems_StorageModels_StorageModelId",
                table: "StorageItems");

            migrationBuilder.DropForeignKey(
                name: "FK_SubSectors_WorldSectorsModels_WorldSectorsModelId",
                table: "SubSectors");

            migrationBuilder.DropForeignKey(
                name: "FK_SubSectorVertices_SubSectors_SubSectorId",
                table: "SubSectorVertices");

            migrationBuilder.DropForeignKey(
                name: "FK_SystemConnections_SystemStarsModels_SystemStarsModelId",
                table: "SystemConnections");

            migrationBuilder.DropForeignKey(
                name: "FK_UserSettingsBurnRateExclusions_UserSettingsBurnRates_UserSettingsBurnRateId",
                table: "UserSettingsBurnRateExclusions");

            migrationBuilder.DropForeignKey(
                name: "FK_UserSettingsBurnRates_UserSettingsModels_UserSettingsModelId",
                table: "UserSettingsBurnRates");

            migrationBuilder.DropForeignKey(
                name: "FK_WorkforceDescriptions_WorkforceModels_WorkforceModelId",
                table: "WorkforceDescriptions");

            migrationBuilder.DropForeignKey(
                name: "FK_WorkforceNeeds_WorkforceDescriptions_WorkforceDescriptionId",
                table: "WorkforceNeeds");

            migrationBuilder.DropTable(
                name: "BUIBuildingCosts");

            migrationBuilder.DropTable(
                name: "BuildingDegradationReclaimableMaterialModels");

            migrationBuilder.DropTable(
                name: "BuildingDegradationRepairMaterialModels");

            migrationBuilder.DropTable(
                name: "BUIRecipeInputs");

            migrationBuilder.DropTable(
                name: "BUIRecipeOutputs");

            migrationBuilder.DropTable(
                name: "CompanyDataCurrencyBalances");

            migrationBuilder.DropTable(
                name: "ContractModels");

            migrationBuilder.DropTable(
                name: "CXOSTradeOrderModels");

            migrationBuilder.DropTable(
                name: "ExpertModels");

            migrationBuilder.DropTable(
                name: "FLIGHTSFlightSegments");

            migrationBuilder.DropTable(
                name: "FXDataModels");

            migrationBuilder.DropTable(
                name: "InfrastructureInfos");

            migrationBuilder.DropTable(
                name: "JumpCacheModels");

            migrationBuilder.DropTable(
                name: "LocalMarketModels");

            migrationBuilder.DropTable(
                name: "MATModels");

            migrationBuilder.DropTable(
                name: "PRODLinesModels");

            migrationBuilder.DropTable(
                name: "SHIPSRepairMaterial");

            migrationBuilder.DropTable(
                name: "SITESReclaimableMaterials");

            migrationBuilder.DropTable(
                name: "SITESRepairMaterials");

            migrationBuilder.DropTable(
                name: "StorageModels");

            migrationBuilder.DropTable(
                name: "SystemStarsModels");

            migrationBuilder.DropTable(
                name: "UserDataModels");

            migrationBuilder.DropTable(
                name: "UserSettingsModels");

            migrationBuilder.DropTable(
                name: "WarehouseModels");

            migrationBuilder.DropTable(
                name: "WorkforceModels");

            migrationBuilder.DropTable(
                name: "WorldSectorsModels");

            migrationBuilder.DropTable(
                name: "BuildingDegradationModels");

            migrationBuilder.DropTable(
                name: "BUIRecipes");

            migrationBuilder.DropTable(
                name: "CompanyDataModels");

            migrationBuilder.DropTable(
                name: "FLIGHTSFlights");

            migrationBuilder.DropTable(
                name: "InfrastructureModels");

            migrationBuilder.DropTable(
                name: "SHIPSShips");

            migrationBuilder.DropTable(
                name: "SITESBuildings");

            migrationBuilder.DropTable(
                name: "BUIModels");

            migrationBuilder.DropTable(
                name: "FLIGHTSModels");

            migrationBuilder.DropTable(
                name: "SHIPSModels");

            migrationBuilder.DropTable(
                name: "SITESSites");

            migrationBuilder.DropTable(
                name: "SITESModels");

            migrationBuilder.DropIndex(
                name: "IX_WorkforceDescriptions_WorkforceModelId",
                table: "WorkforceDescriptions");

            migrationBuilder.DropIndex(
                name: "IX_UserSettingsBurnRates_UserSettingsModelId",
                table: "UserSettingsBurnRates");

            migrationBuilder.DropIndex(
                name: "IX_SystemConnections_SystemStarsModelId",
                table: "SystemConnections");

            migrationBuilder.DropIndex(
                name: "IX_SubSectors_WorldSectorsModelId",
                table: "SubSectors");

            migrationBuilder.DropIndex(
                name: "IX_StorageItems_StorageModelId",
                table: "StorageItems");

            migrationBuilder.DropIndex(
                name: "IX_ShippingAds_LocalMarketModelId",
                table: "ShippingAds");

            migrationBuilder.DropIndex(
                name: "IX_SellingAds_LocalMarketModelId",
                table: "SellingAds");

            migrationBuilder.DropIndex(
                name: "IX_ProductionLines_PRODLinesModelId",
                table: "ProductionLines");

            migrationBuilder.DropIndex(
                name: "IX_JumpCacheRoutes_JumpCacheModelId",
                table: "JumpCacheRoutes");

            migrationBuilder.DropIndex(
                name: "IX_InfrastructureProjectUpkeeps_InfrastructureInfoId",
                table: "InfrastructureProjectUpkeeps");

            migrationBuilder.DropIndex(
                name: "IX_InfrastructureProjectUpgradeCosts_InfrastructureInfoId",
                table: "InfrastructureProjectUpgradeCosts");

            migrationBuilder.DropIndex(
                name: "IX_InfrastructureProjectContributions_InfrastructureInfoId",
                table: "InfrastructureProjectContributions");

            migrationBuilder.DropIndex(
                name: "IX_CXOSTradeOrders_CXOSTradeOrdersModelId",
                table: "CXOSTradeOrders");

            migrationBuilder.DropIndex(
                name: "IX_ContractConditions_ContractModelId",
                table: "ContractConditions");

            migrationBuilder.DropIndex(
                name: "IX_BuyingAds_LocalMarketModelId",
                table: "BuyingAds");

            migrationBuilder.DropPrimaryKey(
                name: "PK_InfrastructureModelReports",
                table: "InfrastructureModelReports");

            migrationBuilder.DropIndex(
                name: "IX_InfrastructureModelReports_InfrastructureModelId",
                table: "InfrastructureModelReports");

            migrationBuilder.DropPrimaryKey(
                name: "PK_FLIGHTSOriginLines",
                table: "FLIGHTSOriginLines");

            migrationBuilder.DropIndex(
                name: "IX_FLIGHTSOriginLines_FLIGHTSFlightSegmentId",
                table: "FLIGHTSOriginLines");

            migrationBuilder.DropPrimaryKey(
                name: "PK_FLIGHTSDestinationLines",
                table: "FLIGHTSDestinationLines");

            migrationBuilder.DropIndex(
                name: "IX_FLIGHTSDestinationLines_FLIGHTSFlightSegmentId",
                table: "FLIGHTSDestinationLines");

            migrationBuilder.DropColumn(
                name: "WorkforceModelId",
                table: "WorkforceDescriptions");

            migrationBuilder.DropColumn(
                name: "UserSettingsModelId",
                table: "UserSettingsBurnRates");

            migrationBuilder.DropColumn(
                name: "SystemStarsModelId",
                table: "SystemConnections");

            migrationBuilder.DropColumn(
                name: "WorldSectorsModelId",
                table: "SubSectors");

            migrationBuilder.DropColumn(
                name: "StorageModelId",
                table: "StorageItems");

            migrationBuilder.DropColumn(
                name: "LocalMarketModelId",
                table: "ShippingAds");

            migrationBuilder.DropColumn(
                name: "LocalMarketModelId",
                table: "SellingAds");

            migrationBuilder.DropColumn(
                name: "PRODLinesModelId",
                table: "ProductionLines");

            migrationBuilder.DropColumn(
                name: "ProductionId",
                table: "ProductionLineOrders");

            migrationBuilder.DropColumn(
                name: "JumpCacheModelId",
                table: "JumpCacheRoutes");

            migrationBuilder.DropColumn(
                name: "InfrastructureInfoId",
                table: "InfrastructureProjectUpkeeps");

            migrationBuilder.DropColumn(
                name: "InfrastructureInfoId",
                table: "InfrastructureProjectUpgradeCosts");

            migrationBuilder.DropColumn(
                name: "InfrastructureInfoId",
                table: "InfrastructureProjectContributions");

            migrationBuilder.DropColumn(
                name: "TradeId",
                table: "CXOSTrades");

            migrationBuilder.DropColumn(
                name: "CXOSTradeOrdersModelId",
                table: "CXOSTradeOrders");

            migrationBuilder.DropColumn(
                name: "CountryId",
                table: "CountryRegistryCountries");

            migrationBuilder.DropColumn(
                name: "ContractModelId",
                table: "ContractConditions");

            migrationBuilder.DropColumn(
                name: "ExchangeId",
                table: "ComexExchanges");

            migrationBuilder.DropColumn(
                name: "LocalMarketModelId",
                table: "BuyingAds");

            migrationBuilder.DropColumn(
                name: "InfrastructureModelId",
                table: "InfrastructureModelReports");

            migrationBuilder.DropColumn(
                name: "FLIGHTSFlightSegmentId",
                table: "FLIGHTSOriginLines");

            migrationBuilder.DropColumn(
                name: "FLIGHTSFlightSegmentId",
                table: "FLIGHTSDestinationLines");

            migrationBuilder.RenameTable(
                name: "InfrastructureModelReports",
                newName: "InfrastructureReports");

            migrationBuilder.RenameTable(
                name: "FLIGHTSOriginLines",
                newName: "OriginLines");

            migrationBuilder.RenameTable(
                name: "FLIGHTSDestinationLines",
                newName: "DestinationLines");

            migrationBuilder.RenameColumn(
                name: "Connection",
                table: "SystemConnections",
                newName: "SystemId");

            migrationBuilder.RenameColumn(
                name: "SSId",
                table: "SubSectors",
                newName: "SectorId");

            migrationBuilder.RenameColumn(
                name: "LineId",
                table: "ProductionLines",
                newName: "UserNameSubmitted");

            migrationBuilder.RenameColumn(
                name: "TradeOrderId",
                table: "CXOSTradeOrders",
                newName: "UserNameSubmitted");

            migrationBuilder.AlterColumn<string>(
                name: "WorkforceDescriptionId",
                table: "WorkforceNeeds",
                type: "TEXT",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AlterColumn<string>(
                name: "WorkforceNeedId",
                table: "WorkforceNeeds",
                type: "TEXT",
                maxLength: 60,
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER")
                .OldAnnotation("Sqlite:Autoincrement", true);

            migrationBuilder.AlterColumn<string>(
                name: "WorkforceDescriptionId",
                table: "WorkforceDescriptions",
                type: "TEXT",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER")
                .OldAnnotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddColumn<string>(
                name: "WorkforceId",
                table: "WorkforceDescriptions",
                type: "TEXT",
                maxLength: 32,
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "UserSettingsBurnRateId",
                table: "UserSettingsBurnRates",
                type: "TEXT",
                maxLength: 41,
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER")
                .OldAnnotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddColumn<string>(
                name: "UserSettingsId",
                table: "UserSettingsBurnRates",
                type: "TEXT",
                maxLength: 32,
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "UserSettingsBurnRateId",
                table: "UserSettingsBurnRateExclusions",
                type: "TEXT",
                maxLength: 41,
                nullable: true,
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AlterColumn<string>(
                name: "UserSettingsBurnRateExclusionId",
                table: "UserSettingsBurnRateExclusions",
                type: "TEXT",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER")
                .OldAnnotation("Sqlite:Autoincrement", true);

            migrationBuilder.AlterColumn<string>(
                name: "SystemStarId",
                table: "SystemStars",
                type: "TEXT",
                maxLength: 32,
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER")
                .OldAnnotation("Sqlite:Autoincrement", true);

            migrationBuilder.AlterColumn<string>(
                name: "SystemConnectionId",
                table: "SystemConnections",
                type: "TEXT",
                maxLength: 32,
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER")
                .OldAnnotation("Sqlite:Autoincrement", true);

            migrationBuilder.AlterColumn<string>(
                name: "SubSectorId",
                table: "SubSectorVertices",
                type: "TEXT",
                maxLength: 32,
                nullable: true,
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AlterColumn<string>(
                name: "SubSectorVertexId",
                table: "SubSectorVertices",
                type: "TEXT",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER")
                .OldAnnotation("Sqlite:Autoincrement", true);

            migrationBuilder.AlterColumn<string>(
                name: "SubSectorId",
                table: "SubSectors",
                type: "TEXT",
                maxLength: 32,
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER")
                .OldAnnotation("Sqlite:Autoincrement", true);

            migrationBuilder.AlterColumn<string>(
                name: "StorageItemId",
                table: "StorageItems",
                type: "TEXT",
                maxLength: 65,
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER")
                .OldAnnotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddColumn<string>(
                name: "StorageId",
                table: "StorageItems",
                type: "TEXT",
                maxLength: 32,
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "StationId",
                table: "Stations",
                type: "TEXT",
                maxLength: 32,
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER")
                .OldAnnotation("Sqlite:Autoincrement", true);

            migrationBuilder.AlterColumn<string>(
                name: "ShippingAdId",
                table: "ShippingAds",
                type: "TEXT",
                maxLength: 64,
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER")
                .OldAnnotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddColumn<string>(
                name: "LocalMarketId",
                table: "ShippingAds",
                type: "TEXT",
                maxLength: 32,
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "SellingAdId",
                table: "SellingAds",
                type: "TEXT",
                maxLength: 64,
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER")
                .OldAnnotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddColumn<string>(
                name: "LocalMarketId",
                table: "SellingAds",
                type: "TEXT",
                maxLength: 32,
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ProductionLineId",
                table: "ProductionLines",
                type: "TEXT",
                maxLength: 32,
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER")
                .OldAnnotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Timestamp",
                table: "ProductionLines",
                type: "TEXT",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AlterColumn<string>(
                name: "ProductionLineOrderId",
                table: "ProductionLineOutputs",
                type: "TEXT",
                maxLength: 32,
                nullable: true,
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AlterColumn<string>(
                name: "ProductionLineOutputId",
                table: "ProductionLineOutputs",
                type: "TEXT",
                maxLength: 41,
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER")
                .OldAnnotation("Sqlite:Autoincrement", true);

            migrationBuilder.AlterColumn<string>(
                name: "ProductionLineId",
                table: "ProductionLineOrders",
                type: "TEXT",
                maxLength: 32,
                nullable: true,
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AlterColumn<string>(
                name: "ProductionLineOrderId",
                table: "ProductionLineOrders",
                type: "TEXT",
                maxLength: 32,
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER")
                .OldAnnotation("Sqlite:Autoincrement", true);

            migrationBuilder.AlterColumn<string>(
                name: "ProductionLineOrderId",
                table: "ProductionLineInputs",
                type: "TEXT",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AlterColumn<string>(
                name: "ProductionLineInputId",
                table: "ProductionLineInputs",
                type: "TEXT",
                maxLength: 41,
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER")
                .OldAnnotation("Sqlite:Autoincrement", true);

            migrationBuilder.AlterColumn<string>(
                name: "JumpCacheRouteJumpId",
                table: "JumpCacheRoutes",
                type: "TEXT",
                maxLength: 131,
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER")
                .OldAnnotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddColumn<string>(
                name: "JumpCacheId",
                table: "JumpCacheRoutes",
                type: "TEXT",
                maxLength: 65,
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "InfrastructureProjectUpkeepsId",
                table: "InfrastructureProjectUpkeeps",
                type: "TEXT",
                maxLength: 128,
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER")
                .OldAnnotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddColumn<string>(
                name: "InfrastructureProjectId",
                table: "InfrastructureProjectUpkeeps",
                type: "TEXT",
                maxLength: 64,
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "InfrastructureProjectUpgradeCostsId",
                table: "InfrastructureProjectUpgradeCosts",
                type: "TEXT",
                maxLength: 128,
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER")
                .OldAnnotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddColumn<string>(
                name: "InfrastructureProjectId",
                table: "InfrastructureProjectUpgradeCosts",
                type: "TEXT",
                maxLength: 64,
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "InfrastructureProjectContributionsId",
                table: "InfrastructureProjectContributions",
                type: "TEXT",
                maxLength: 128,
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER")
                .OldAnnotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddColumn<string>(
                name: "InfrastructureProjectId",
                table: "InfrastructureProjectContributions",
                type: "TEXT",
                maxLength: 64,
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CXPCDataId",
                table: "CXPCDataEntries",
                type: "TEXT",
                maxLength: 32,
                nullable: true,
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AlterColumn<string>(
                name: "CXPCDataEntryId",
                table: "CXPCDataEntries",
                type: "TEXT",
                maxLength: 64,
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER")
                .OldAnnotation("Sqlite:Autoincrement", true);

            migrationBuilder.AlterColumn<string>(
                name: "CXPCDataId",
                table: "CXPCData",
                type: "TEXT",
                maxLength: 32,
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER")
                .OldAnnotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Timestamp",
                table: "CXPCData",
                type: "TEXT",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "UserNameSubmitted",
                table: "CXPCData",
                type: "TEXT",
                maxLength: 32,
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CXOSTradeOrderId",
                table: "CXOSTrades",
                type: "TEXT",
                maxLength: 32,
                nullable: true,
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AlterColumn<string>(
                name: "CXOSTradeId",
                table: "CXOSTrades",
                type: "TEXT",
                maxLength: 32,
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER")
                .OldAnnotation("Sqlite:Autoincrement", true);

            migrationBuilder.AlterColumn<string>(
                name: "CXOSTradeOrderId",
                table: "CXOSTradeOrders",
                type: "TEXT",
                maxLength: 32,
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER")
                .OldAnnotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Timestamp",
                table: "CXOSTradeOrders",
                type: "TEXT",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AlterColumn<string>(
                name: "CountryRegistryCountryId",
                table: "CountryRegistryCountries",
                type: "TEXT",
                maxLength: 32,
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER")
                .OldAnnotation("Sqlite:Autoincrement", true);

            migrationBuilder.AlterColumn<string>(
                name: "ContractConditionId",
                table: "ContractDependencies",
                type: "TEXT",
                maxLength: 40,
                nullable: true,
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AlterColumn<string>(
                name: "ContractDependencyId",
                table: "ContractDependencies",
                type: "TEXT",
                maxLength: 80,
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER")
                .OldAnnotation("Sqlite:Autoincrement", true);

            migrationBuilder.AlterColumn<string>(
                name: "ContractConditionId",
                table: "ContractConditions",
                type: "TEXT",
                maxLength: 40,
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER")
                .OldAnnotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddColumn<string>(
                name: "ContractId",
                table: "ContractConditions",
                type: "TEXT",
                maxLength: 32,
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ComexExchangeId",
                table: "ComexExchanges",
                type: "TEXT",
                maxLength: 32,
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER")
                .OldAnnotation("Sqlite:Autoincrement", true);

            migrationBuilder.AlterColumn<string>(
                name: "BuyingAdId",
                table: "BuyingAds",
                type: "TEXT",
                maxLength: 64,
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER")
                .OldAnnotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddColumn<string>(
                name: "LocalMarketId",
                table: "BuyingAds",
                type: "TEXT",
                maxLength: 32,
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "InfrastructureReportId",
                table: "InfrastructureReports",
                type: "TEXT",
                maxLength: 40,
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER")
                .OldAnnotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddColumn<string>(
                name: "InfrastructureId",
                table: "InfrastructureReports",
                type: "TEXT",
                maxLength: 32,
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "OriginLineId",
                table: "OriginLines",
                type: "TEXT",
                maxLength: 128,
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER")
                .OldAnnotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddColumn<string>(
                name: "FlightSegmentId",
                table: "OriginLines",
                type: "TEXT",
                maxLength: 64,
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "DestinationLineId",
                table: "DestinationLines",
                type: "TEXT",
                maxLength: 128,
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER")
                .OldAnnotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddColumn<string>(
                name: "FlightSegmentId",
                table: "DestinationLines",
                type: "TEXT",
                maxLength: 64,
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_InfrastructureReports",
                table: "InfrastructureReports",
                column: "InfrastructureReportId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_OriginLines",
                table: "OriginLines",
                column: "OriginLineId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_DestinationLines",
                table: "DestinationLines",
                column: "DestinationLineId");

            migrationBuilder.CreateTable(
                name: "BuildingDegradations",
                columns: table => new
                {
                    BuildingDegradationId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    BuildingTicker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    BuildingCreated = table.Column<long>(type: "INTEGER", nullable: false),
                    HasBeenRepaired = table.Column<bool>(type: "INTEGER", nullable: false),
                    Condition = table.Column<double>(type: "REAL", nullable: false),
                    DaysSinceLastRepair = table.Column<double>(type: "REAL", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuildingDegradations", x => x.BuildingDegradationId);
                });

            migrationBuilder.CreateTable(
                name: "Buildings",
                columns: table => new
                {
                    BuildingId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    Name = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    Ticker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    Expertise = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Pioneers = table.Column<int>(type: "INTEGER", nullable: false),
                    Settlers = table.Column<int>(type: "INTEGER", nullable: false),
                    Technicians = table.Column<int>(type: "INTEGER", nullable: false),
                    Engineers = table.Column<int>(type: "INTEGER", nullable: false),
                    Scientists = table.Column<int>(type: "INTEGER", nullable: false),
                    AreaCost = table.Column<int>(type: "INTEGER", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Buildings", x => x.BuildingId);
                });

            migrationBuilder.CreateTable(
                name: "Companies",
                columns: table => new
                {
                    CompanyId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    CompanyName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    CompanyCode = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    UserName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    HighestTier = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Pioneer = table.Column<bool>(type: "INTEGER", nullable: false),
                    Team = table.Column<bool>(type: "INTEGER", nullable: false),
                    CreatedEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    CountryId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    CurrencyCode = table.Column<string>(type: "TEXT", maxLength: 4, nullable: true),
                    StartingProfile = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    StartingLocation = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    OverallRating = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    ActivityRating = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    ReliabilityRating = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    StabilityRating = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Companies", x => x.CompanyId);
                });

            migrationBuilder.CreateTable(
                name: "Contracts",
                columns: table => new
                {
                    ContractId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    ContractLocalId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    DateEpochMs = table.Column<long>(type: "INTEGER", nullable: true),
                    ExtensionDeadlineEpochMs = table.Column<long>(type: "INTEGER", nullable: true),
                    DueDateEpochMs = table.Column<long>(type: "INTEGER", nullable: true),
                    CanExtend = table.Column<bool>(type: "INTEGER", nullable: false),
                    Party = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Status = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    PartnerId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    PartnerName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    PartnerCompanyCode = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contracts", x => x.ContractId);
                });

            migrationBuilder.CreateTable(
                name: "Experts",
                columns: table => new
                {
                    ExpertsId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    PlanetId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    AgricultureActive = table.Column<int>(type: "INTEGER", nullable: false),
                    AgricultureAvailable = table.Column<int>(type: "INTEGER", nullable: false),
                    AgricultureEfficiencyGain = table.Column<double>(type: "REAL", nullable: false),
                    ResourceExtractioneActive = table.Column<int>(type: "INTEGER", nullable: false),
                    ResourceExtractionAvailable = table.Column<int>(type: "INTEGER", nullable: false),
                    ResourceExtractionEfficiencyGain = table.Column<double>(type: "REAL", nullable: false),
                    FoodIndustriesActive = table.Column<int>(type: "INTEGER", nullable: false),
                    FoodIndustriesAvailable = table.Column<int>(type: "INTEGER", nullable: false),
                    FoodIndustriesEfficiencyGain = table.Column<double>(type: "REAL", nullable: false),
                    ChemistryActive = table.Column<int>(type: "INTEGER", nullable: false),
                    ChemistryAvailable = table.Column<int>(type: "INTEGER", nullable: false),
                    ChemistryEfficiencyGain = table.Column<double>(type: "REAL", nullable: false),
                    ConstructionActive = table.Column<int>(type: "INTEGER", nullable: false),
                    ConstructionAvailable = table.Column<int>(type: "INTEGER", nullable: false),
                    ConstructionEfficiencyGain = table.Column<double>(type: "REAL", nullable: false),
                    ElectronicsActive = table.Column<int>(type: "INTEGER", nullable: false),
                    ElectronicsAvailable = table.Column<int>(type: "INTEGER", nullable: false),
                    ElectronicsEfficiencyGain = table.Column<double>(type: "REAL", nullable: false),
                    FuelRefiningActive = table.Column<int>(type: "INTEGER", nullable: false),
                    FuelRefiningAvailable = table.Column<int>(type: "INTEGER", nullable: false),
                    FuelRefiningEfficiencyGain = table.Column<double>(type: "REAL", nullable: false),
                    ManufacturingActive = table.Column<int>(type: "INTEGER", nullable: false),
                    ManufacturingAvailable = table.Column<int>(type: "INTEGER", nullable: false),
                    ManufacturingEfficiencyGain = table.Column<double>(type: "REAL", nullable: false),
                    MetallurgyActive = table.Column<int>(type: "INTEGER", nullable: false),
                    MetallurgyAvailable = table.Column<int>(type: "INTEGER", nullable: false),
                    MetallurgyEfficiencyGain = table.Column<double>(type: "REAL", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Experts", x => x.ExpertsId);
                });

            migrationBuilder.CreateTable(
                name: "Flights",
                columns: table => new
                {
                    FlightId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    ShipId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Origin = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    Destination = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    DepartureTimeEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    ArrivalTimeEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    CurrentSegmentIndex = table.Column<int>(type: "INTEGER", nullable: false),
                    StlDistance = table.Column<double>(type: "REAL", nullable: false),
                    FtlDistance = table.Column<double>(type: "REAL", nullable: false),
                    IsAborted = table.Column<bool>(type: "INTEGER", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Flights", x => x.FlightId);
                });

            migrationBuilder.CreateTable(
                name: "FXDataPairs",
                columns: table => new
                {
                    FXPairId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    BaseCurrencyCode = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    BaseCurrencyName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    BaseCurrencyNumericCode = table.Column<int>(type: "INTEGER", nullable: false),
                    QuoteCurrencyCode = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    QuoteCurrencyName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    QuoteCurrencyNumericCode = table.Column<int>(type: "INTEGER", nullable: false),
                    High = table.Column<decimal>(type: "TEXT", nullable: false),
                    Low = table.Column<decimal>(type: "TEXT", nullable: false),
                    Open = table.Column<decimal>(type: "TEXT", nullable: false),
                    Previous = table.Column<decimal>(type: "TEXT", nullable: false),
                    Traded = table.Column<decimal>(type: "TEXT", nullable: false),
                    Volume = table.Column<decimal>(type: "TEXT", nullable: false),
                    PriceUpdateEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FXDataPairs", x => x.FXPairId);
                });

            migrationBuilder.CreateTable(
                name: "Infrastructures",
                columns: table => new
                {
                    InfrastructureId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Infrastructures", x => x.InfrastructureId);
                });

            migrationBuilder.CreateTable(
                name: "JumpCache",
                columns: table => new
                {
                    JumpCacheId = table.Column<string>(type: "TEXT", maxLength: 65, nullable: false),
                    SourceSystemId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    SourceSystemName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    SourceSystemNaturalId = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    DestinationSystemId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    DestinationSystemName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    DestinationNaturalId = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    OverallDistance = table.Column<double>(type: "REAL", nullable: false),
                    JumpCount = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JumpCache", x => x.JumpCacheId);
                });

            migrationBuilder.CreateTable(
                name: "LocalMarkets",
                columns: table => new
                {
                    LocalMarketId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LocalMarkets", x => x.LocalMarketId);
                });

            migrationBuilder.CreateTable(
                name: "Materials",
                columns: table => new
                {
                    MaterialId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    CategoryName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    CategoryId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Name = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    Ticker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    Weight = table.Column<double>(type: "REAL", nullable: false),
                    Volume = table.Column<double>(type: "REAL", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Materials", x => x.MaterialId);
                });

            migrationBuilder.CreateTable(
                name: "Sectors",
                columns: table => new
                {
                    SectorId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    Name = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    HexQ = table.Column<int>(type: "INTEGER", nullable: false),
                    HexR = table.Column<int>(type: "INTEGER", nullable: false),
                    HexS = table.Column<int>(type: "INTEGER", nullable: false),
                    Size = table.Column<int>(type: "INTEGER", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sectors", x => x.SectorId);
                });

            migrationBuilder.CreateTable(
                name: "Ships",
                columns: table => new
                {
                    ShipId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    StoreId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    StlFuelStoreId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    FtlFuelStoreId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Registration = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Name = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    CommissioningTimeEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    BlueprintNaturalId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    FlightId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Acceleration = table.Column<double>(type: "REAL", nullable: false),
                    Thrust = table.Column<double>(type: "REAL", nullable: false),
                    Mass = table.Column<double>(type: "REAL", nullable: false),
                    OperatingEmptyMass = table.Column<double>(type: "REAL", nullable: false),
                    ReactorPower = table.Column<double>(type: "REAL", nullable: false),
                    EmitterPower = table.Column<double>(type: "REAL", nullable: false),
                    Volume = table.Column<double>(type: "REAL", nullable: false),
                    Condition = table.Column<double>(type: "REAL", nullable: false),
                    LastRepairEpochMs = table.Column<long>(type: "INTEGER", nullable: true),
                    Location = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    StlFuelFlowRate = table.Column<double>(type: "REAL", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ships", x => x.ShipId);
                });

            migrationBuilder.CreateTable(
                name: "Sites",
                columns: table => new
                {
                    SiteId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    PlanetId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    PlanetIdentifier = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    PlanetName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    PlanetFoundedEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sites", x => x.SiteId);
                });

            migrationBuilder.CreateTable(
                name: "Storages",
                columns: table => new
                {
                    StorageId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    AddressableId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Name = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    WeightLoad = table.Column<double>(type: "REAL", nullable: false),
                    WeightCapacity = table.Column<double>(type: "REAL", nullable: false),
                    VolumeLoad = table.Column<double>(type: "REAL", nullable: false),
                    VolumeCapacity = table.Column<double>(type: "REAL", nullable: false),
                    FixedStore = table.Column<bool>(type: "INTEGER", nullable: false),
                    Type = table.Column<string>(type: "TEXT", maxLength: 16, nullable: true),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Storages", x => x.StorageId);
                });

            migrationBuilder.CreateTable(
                name: "Systems",
                columns: table => new
                {
                    SystemId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    Name = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    NaturalId = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    Type = table.Column<string>(type: "TEXT", maxLength: 16, nullable: true),
                    PositionX = table.Column<double>(type: "REAL", nullable: false),
                    PositionY = table.Column<double>(type: "REAL", nullable: false),
                    PositionZ = table.Column<double>(type: "REAL", nullable: false),
                    SectorId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    SubSectorId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Systems", x => x.SystemId);
                });

            migrationBuilder.CreateTable(
                name: "UserData",
                columns: table => new
                {
                    UserDataId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    UserName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Tier = table.Column<string>(type: "TEXT", maxLength: 16, nullable: true),
                    Team = table.Column<bool>(type: "INTEGER", nullable: false),
                    Pioneer = table.Column<bool>(type: "INTEGER", nullable: false),
                    SystemNamingRights = table.Column<int>(type: "INTEGER", nullable: false),
                    PlanetNamingRights = table.Column<int>(type: "INTEGER", nullable: false),
                    IsPayingUser = table.Column<bool>(type: "INTEGER", nullable: false),
                    IsModeratorChat = table.Column<bool>(type: "INTEGER", nullable: false),
                    CreatedEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserData", x => x.UserDataId);
                });

            migrationBuilder.CreateTable(
                name: "UserSettings",
                columns: table => new
                {
                    UserSettingsId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    UserName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserSettings", x => x.UserSettingsId);
                });

            migrationBuilder.CreateTable(
                name: "Warehouses",
                columns: table => new
                {
                    WarehouseId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    StoreId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Units = table.Column<int>(type: "INTEGER", nullable: false),
                    WeightCapacity = table.Column<double>(type: "REAL", nullable: false),
                    VolumeCapacity = table.Column<double>(type: "REAL", nullable: false),
                    NextPaymentTimestampEpochMs = table.Column<long>(type: "INTEGER", nullable: true),
                    FeeAmount = table.Column<double>(type: "REAL", nullable: true),
                    FeeCurrency = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    FeeCollectorId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    FeeCollectorName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    FeeCollectorCode = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    LocationName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    LocationNaturalId = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Warehouses", x => x.WarehouseId);
                });

            migrationBuilder.CreateTable(
                name: "Workforces",
                columns: table => new
                {
                    WorkforceId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    PlanetId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    PlanetNaturalId = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    PlanetName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    SiteId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    LastWorkforceUpdateTime = table.Column<DateTime>(type: "TEXT", nullable: true),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Workforces", x => x.WorkforceId);
                });

            migrationBuilder.CreateTable(
                name: "BuildingDegradationReclaimableMaterials",
                columns: table => new
                {
                    BuildingDegradationReclaimableMaterialModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    MaterialTicker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    MaterialCount = table.Column<int>(type: "INTEGER", nullable: false),
                    BuildingDegradationId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuildingDegradationReclaimableMaterials", x => x.BuildingDegradationReclaimableMaterialModelId);
                    table.ForeignKey(
                        name: "FK_BuildingDegradationReclaimableMaterials_BuildingDegradations_BuildingDegradationId",
                        column: x => x.BuildingDegradationId,
                        principalTable: "BuildingDegradations",
                        principalColumn: "BuildingDegradationId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BuildingDegradationRepairMaterials",
                columns: table => new
                {
                    BuildingDegradationRepairMaterialId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    MaterialTicker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    MaterialCount = table.Column<int>(type: "INTEGER", nullable: false),
                    BuildingDegradationId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuildingDegradationRepairMaterials", x => x.BuildingDegradationRepairMaterialId);
                    table.ForeignKey(
                        name: "FK_BuildingDegradationRepairMaterials_BuildingDegradations_BuildingDegradationId",
                        column: x => x.BuildingDegradationId,
                        principalTable: "BuildingDegradations",
                        principalColumn: "BuildingDegradationId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BuildingCosts",
                columns: table => new
                {
                    BuildingCostModelId = table.Column<string>(type: "TEXT", maxLength: 40, nullable: false),
                    CommodityName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    CommodityTicker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    Weight = table.Column<double>(type: "REAL", nullable: false),
                    Volume = table.Column<double>(type: "REAL", nullable: false),
                    Amount = table.Column<int>(type: "INTEGER", nullable: false),
                    BuildingId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuildingCosts", x => x.BuildingCostModelId);
                    table.ForeignKey(
                        name: "FK_BuildingCosts_Buildings_BuildingId",
                        column: x => x.BuildingId,
                        principalTable: "Buildings",
                        principalColumn: "BuildingId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BuildingRecipes",
                columns: table => new
                {
                    BuildingRecipeModelId = table.Column<string>(type: "TEXT", maxLength: 64, nullable: false),
                    DurationMs = table.Column<int>(type: "INTEGER", nullable: false),
                    RecipeName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    BuildingId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuildingRecipes", x => x.BuildingRecipeModelId);
                    table.ForeignKey(
                        name: "FK_BuildingRecipes_Buildings_BuildingId",
                        column: x => x.BuildingId,
                        principalTable: "Buildings",
                        principalColumn: "BuildingId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CompanyCurrencyBalances",
                columns: table => new
                {
                    CompanyCurrencyBalanceId = table.Column<string>(type: "TEXT", maxLength: 40, nullable: false),
                    Currency = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    Balance = table.Column<double>(type: "REAL", nullable: false),
                    CompanyId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompanyCurrencyBalances", x => x.CompanyCurrencyBalanceId);
                    table.ForeignKey(
                        name: "FK_CompanyCurrencyBalances_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "CompanyId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FlightSegments",
                columns: table => new
                {
                    FlightSegmentId = table.Column<string>(type: "TEXT", maxLength: 64, nullable: false),
                    Type = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    DepartureTimeEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    ArrivalTimeEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    StlDistance = table.Column<double>(type: "REAL", nullable: true),
                    StlFuelConsumption = table.Column<double>(type: "REAL", nullable: true),
                    FtlDistance = table.Column<double>(type: "REAL", nullable: true),
                    FtlFuelConsumption = table.Column<double>(type: "REAL", nullable: true),
                    Origin = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    Destination = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    FlightId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FlightSegments", x => x.FlightSegmentId);
                    table.ForeignKey(
                        name: "FK_FlightSegments_Flights_FlightId",
                        column: x => x.FlightId,
                        principalTable: "Flights",
                        principalColumn: "FlightId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "InfrastructureProjects",
                columns: table => new
                {
                    InfrastructureProjectId = table.Column<string>(type: "TEXT", maxLength: 64, nullable: false),
                    InfraProjectId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    SimulationPeriod = table.Column<int>(type: "INTEGER", nullable: false),
                    Type = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Ticker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    Name = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    Level = table.Column<int>(type: "INTEGER", nullable: false),
                    ActiveLevel = table.Column<int>(type: "INTEGER", nullable: false),
                    CurrentLevel = table.Column<int>(type: "INTEGER", nullable: false),
                    UpkeepStatus = table.Column<double>(type: "REAL", nullable: false),
                    UpgradeStatus = table.Column<double>(type: "REAL", nullable: false),
                    InfrastructureId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InfrastructureProjects", x => x.InfrastructureProjectId);
                    table.ForeignKey(
                        name: "FK_InfrastructureProjects_Infrastructures_InfrastructureId",
                        column: x => x.InfrastructureId,
                        principalTable: "Infrastructures",
                        principalColumn: "InfrastructureId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ShipRepairMaterials",
                columns: table => new
                {
                    ShipRepairMaterialId = table.Column<string>(type: "TEXT", maxLength: 41, nullable: false),
                    MaterialName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    MaterialId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    MaterialTicker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    Amount = table.Column<int>(type: "INTEGER", nullable: false),
                    ShipId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShipRepairMaterials", x => x.ShipRepairMaterialId);
                    table.ForeignKey(
                        name: "FK_ShipRepairMaterials_Ships_ShipId",
                        column: x => x.ShipId,
                        principalTable: "Ships",
                        principalColumn: "ShipId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SiteBuildings",
                columns: table => new
                {
                    SiteBuildingId = table.Column<string>(type: "TEXT", maxLength: 65, nullable: false),
                    BuildingId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    BuildingCreated = table.Column<long>(type: "INTEGER", nullable: false),
                    BuildingName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    BuildingTicker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    BuildingLastRepair = table.Column<long>(type: "INTEGER", nullable: true),
                    Condition = table.Column<double>(type: "REAL", nullable: false),
                    SiteId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SiteBuildings", x => x.SiteBuildingId);
                    table.ForeignKey(
                        name: "FK_SiteBuildings_Sites_SiteId",
                        column: x => x.SiteId,
                        principalTable: "Sites",
                        principalColumn: "SiteId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BuildingRecipeInputs",
                columns: table => new
                {
                    BuildingRecipeInputModelId = table.Column<string>(type: "TEXT", maxLength: 60, nullable: false),
                    CommodityName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    CommodityTicker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    Weight = table.Column<double>(type: "REAL", nullable: false),
                    Volume = table.Column<double>(type: "REAL", nullable: false),
                    Amount = table.Column<int>(type: "INTEGER", nullable: false),
                    BuildingRecipeModelId = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuildingRecipeInputs", x => x.BuildingRecipeInputModelId);
                    table.ForeignKey(
                        name: "FK_BuildingRecipeInputs_BuildingRecipes_BuildingRecipeModelId",
                        column: x => x.BuildingRecipeModelId,
                        principalTable: "BuildingRecipes",
                        principalColumn: "BuildingRecipeModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BuildingRecipeOutputs",
                columns: table => new
                {
                    BuildingRecipeOutputModelId = table.Column<string>(type: "TEXT", maxLength: 60, nullable: false),
                    CommodityName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    CommodityTicker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    Weight = table.Column<double>(type: "REAL", nullable: false),
                    Volume = table.Column<double>(type: "REAL", nullable: false),
                    Amount = table.Column<int>(type: "INTEGER", nullable: false),
                    BuildingRecipeModelId = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuildingRecipeOutputs", x => x.BuildingRecipeOutputModelId);
                    table.ForeignKey(
                        name: "FK_BuildingRecipeOutputs_BuildingRecipes_BuildingRecipeModelId",
                        column: x => x.BuildingRecipeModelId,
                        principalTable: "BuildingRecipes",
                        principalColumn: "BuildingRecipeModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SiteReclaimableMaterials",
                columns: table => new
                {
                    SiteReclaimableMaterialId = table.Column<string>(type: "TEXT", maxLength: 75, nullable: false),
                    MaterialId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    MaterialName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    MaterialTicker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    MaterialAmount = table.Column<int>(type: "INTEGER", nullable: false),
                    SiteBuildingId = table.Column<string>(type: "TEXT", maxLength: 65, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SiteReclaimableMaterials", x => x.SiteReclaimableMaterialId);
                    table.ForeignKey(
                        name: "FK_SiteReclaimableMaterials_SiteBuildings_SiteBuildingId",
                        column: x => x.SiteBuildingId,
                        principalTable: "SiteBuildings",
                        principalColumn: "SiteBuildingId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SiteRepairMaterials",
                columns: table => new
                {
                    SiteRepairMaterialId = table.Column<string>(type: "TEXT", maxLength: 75, nullable: false),
                    MaterialId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    MaterialName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    MaterialTicker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    MaterialAmount = table.Column<int>(type: "INTEGER", nullable: false),
                    SiteBuildingId = table.Column<string>(type: "TEXT", maxLength: 65, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SiteRepairMaterials", x => x.SiteRepairMaterialId);
                    table.ForeignKey(
                        name: "FK_SiteRepairMaterials_SiteBuildings_SiteBuildingId",
                        column: x => x.SiteBuildingId,
                        principalTable: "SiteBuildings",
                        principalColumn: "SiteBuildingId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WorkforceDescriptions_WorkforceId",
                table: "WorkforceDescriptions",
                column: "WorkforceId");

            migrationBuilder.CreateIndex(
                name: "IX_UserSettingsBurnRates_UserSettingsId",
                table: "UserSettingsBurnRates",
                column: "UserSettingsId");

            migrationBuilder.CreateIndex(
                name: "IX_SystemStars_SystemId",
                table: "SystemStars",
                column: "SystemId");

            migrationBuilder.CreateIndex(
                name: "IX_SystemStars_SystemName",
                table: "SystemStars",
                column: "SystemName");

            migrationBuilder.CreateIndex(
                name: "IX_SystemStars_SystemNaturalId",
                table: "SystemStars",
                column: "SystemNaturalId");

            migrationBuilder.CreateIndex(
                name: "IX_SystemConnections_SystemId",
                table: "SystemConnections",
                column: "SystemId");

            migrationBuilder.CreateIndex(
                name: "IX_SubSectors_SectorId",
                table: "SubSectors",
                column: "SectorId");

            migrationBuilder.CreateIndex(
                name: "IX_StorageItems_StorageId",
                table: "StorageItems",
                column: "StorageId");

            migrationBuilder.CreateIndex(
                name: "IX_ShippingAds_LocalMarketId",
                table: "ShippingAds",
                column: "LocalMarketId");

            migrationBuilder.CreateIndex(
                name: "IX_ShippingAds_PlanetId",
                table: "ShippingAds",
                column: "PlanetId");

            migrationBuilder.CreateIndex(
                name: "IX_ShippingAds_PlanetName",
                table: "ShippingAds",
                column: "PlanetName");

            migrationBuilder.CreateIndex(
                name: "IX_ShippingAds_PlanetNaturalId",
                table: "ShippingAds",
                column: "PlanetNaturalId");

            migrationBuilder.CreateIndex(
                name: "IX_SellingAds_LocalMarketId",
                table: "SellingAds",
                column: "LocalMarketId");

            migrationBuilder.CreateIndex(
                name: "IX_SellingAds_PlanetId",
                table: "SellingAds",
                column: "PlanetId");

            migrationBuilder.CreateIndex(
                name: "IX_SellingAds_PlanetName",
                table: "SellingAds",
                column: "PlanetName");

            migrationBuilder.CreateIndex(
                name: "IX_SellingAds_PlanetNaturalId",
                table: "SellingAds",
                column: "PlanetNaturalId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductionLines_PlanetId",
                table: "ProductionLines",
                column: "PlanetId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductionLines_PlanetName",
                table: "ProductionLines",
                column: "PlanetName");

            migrationBuilder.CreateIndex(
                name: "IX_ProductionLines_PlanetNaturalId",
                table: "ProductionLines",
                column: "PlanetNaturalId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductionLines_SiteId",
                table: "ProductionLines",
                column: "SiteId");

            migrationBuilder.CreateIndex(
                name: "IX_JumpCacheRoutes_DestinationSystemId",
                table: "JumpCacheRoutes",
                column: "DestinationSystemId");

            migrationBuilder.CreateIndex(
                name: "IX_JumpCacheRoutes_JumpCacheId",
                table: "JumpCacheRoutes",
                column: "JumpCacheId");

            migrationBuilder.CreateIndex(
                name: "IX_JumpCacheRoutes_SourceSystemId",
                table: "JumpCacheRoutes",
                column: "SourceSystemId");

            migrationBuilder.CreateIndex(
                name: "IX_InfrastructureProjectUpkeeps_InfrastructureProjectId",
                table: "InfrastructureProjectUpkeeps",
                column: "InfrastructureProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_InfrastructureProjectUpgradeCosts_InfrastructureProjectId",
                table: "InfrastructureProjectUpgradeCosts",
                column: "InfrastructureProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_InfrastructureProjectContributions_InfrastructureProjectId",
                table: "InfrastructureProjectContributions",
                column: "InfrastructureProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_ContractConditions_ContractId",
                table: "ContractConditions",
                column: "ContractId");

            migrationBuilder.CreateIndex(
                name: "IX_BuyingAds_LocalMarketId",
                table: "BuyingAds",
                column: "LocalMarketId");

            migrationBuilder.CreateIndex(
                name: "IX_BuyingAds_PlanetId",
                table: "BuyingAds",
                column: "PlanetId");

            migrationBuilder.CreateIndex(
                name: "IX_BuyingAds_PlanetName",
                table: "BuyingAds",
                column: "PlanetName");

            migrationBuilder.CreateIndex(
                name: "IX_BuyingAds_PlanetNaturalId",
                table: "BuyingAds",
                column: "PlanetNaturalId");

            migrationBuilder.CreateIndex(
                name: "IX_InfrastructureReports_InfrastructureId",
                table: "InfrastructureReports",
                column: "InfrastructureId");

            migrationBuilder.CreateIndex(
                name: "IX_OriginLines_FlightSegmentId",
                table: "OriginLines",
                column: "FlightSegmentId");

            migrationBuilder.CreateIndex(
                name: "IX_DestinationLines_FlightSegmentId",
                table: "DestinationLines",
                column: "FlightSegmentId");

            migrationBuilder.CreateIndex(
                name: "IX_BuildingCosts_BuildingId",
                table: "BuildingCosts",
                column: "BuildingId");

            migrationBuilder.CreateIndex(
                name: "IX_BuildingDegradationReclaimableMaterials_BuildingDegradationId_MaterialTicker",
                table: "BuildingDegradationReclaimableMaterials",
                columns: new[] { "BuildingDegradationId", "MaterialTicker" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BuildingDegradationRepairMaterials_BuildingDegradationId_MaterialTicker",
                table: "BuildingDegradationRepairMaterials",
                columns: new[] { "BuildingDegradationId", "MaterialTicker" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BuildingDegradations_BuildingTicker_DaysSinceLastRepair_HasBeenRepaired",
                table: "BuildingDegradations",
                columns: new[] { "BuildingTicker", "DaysSinceLastRepair", "HasBeenRepaired" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BuildingRecipeInputs_BuildingRecipeModelId",
                table: "BuildingRecipeInputs",
                column: "BuildingRecipeModelId");

            migrationBuilder.CreateIndex(
                name: "IX_BuildingRecipeOutputs_BuildingRecipeModelId",
                table: "BuildingRecipeOutputs",
                column: "BuildingRecipeModelId");

            migrationBuilder.CreateIndex(
                name: "IX_BuildingRecipes_BuildingId",
                table: "BuildingRecipes",
                column: "BuildingId");

            migrationBuilder.CreateIndex(
                name: "IX_Buildings_Ticker",
                table: "Buildings",
                column: "Ticker");

            migrationBuilder.CreateIndex(
                name: "IX_Companies_CompanyCode",
                table: "Companies",
                column: "CompanyCode");

            migrationBuilder.CreateIndex(
                name: "IX_Companies_CompanyName",
                table: "Companies",
                column: "CompanyName");

            migrationBuilder.CreateIndex(
                name: "IX_Companies_UserName",
                table: "Companies",
                column: "UserName");

            migrationBuilder.CreateIndex(
                name: "IX_CompanyCurrencyBalances_CompanyId",
                table: "CompanyCurrencyBalances",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Contracts_ContractLocalId",
                table: "Contracts",
                column: "ContractLocalId");

            migrationBuilder.CreateIndex(
                name: "IX_Contracts_PartnerCompanyCode",
                table: "Contracts",
                column: "PartnerCompanyCode");

            migrationBuilder.CreateIndex(
                name: "IX_Contracts_PartnerName",
                table: "Contracts",
                column: "PartnerName");

            migrationBuilder.CreateIndex(
                name: "IX_Contracts_Party",
                table: "Contracts",
                column: "Party");

            migrationBuilder.CreateIndex(
                name: "IX_Flights_ShipId",
                table: "Flights",
                column: "ShipId");

            migrationBuilder.CreateIndex(
                name: "IX_FlightSegments_FlightId",
                table: "FlightSegments",
                column: "FlightId");

            migrationBuilder.CreateIndex(
                name: "IX_InfrastructureProjects_InfrastructureId",
                table: "InfrastructureProjects",
                column: "InfrastructureId");

            migrationBuilder.CreateIndex(
                name: "IX_JumpCache_DestinationNaturalId",
                table: "JumpCache",
                column: "DestinationNaturalId");

            migrationBuilder.CreateIndex(
                name: "IX_JumpCache_DestinationSystemId",
                table: "JumpCache",
                column: "DestinationSystemId");

            migrationBuilder.CreateIndex(
                name: "IX_JumpCache_DestinationSystemName",
                table: "JumpCache",
                column: "DestinationSystemName");

            migrationBuilder.CreateIndex(
                name: "IX_JumpCache_SourceSystemId",
                table: "JumpCache",
                column: "SourceSystemId");

            migrationBuilder.CreateIndex(
                name: "IX_JumpCache_SourceSystemName",
                table: "JumpCache",
                column: "SourceSystemName");

            migrationBuilder.CreateIndex(
                name: "IX_JumpCache_SourceSystemNaturalId",
                table: "JumpCache",
                column: "SourceSystemNaturalId");

            migrationBuilder.CreateIndex(
                name: "IX_Materials_Ticker",
                table: "Materials",
                column: "Ticker");

            migrationBuilder.CreateIndex(
                name: "IX_Sectors_Name",
                table: "Sectors",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_ShipRepairMaterials_ShipId",
                table: "ShipRepairMaterials",
                column: "ShipId");

            migrationBuilder.CreateIndex(
                name: "IX_Ships_FtlFuelStoreId",
                table: "Ships",
                column: "FtlFuelStoreId");

            migrationBuilder.CreateIndex(
                name: "IX_Ships_Name",
                table: "Ships",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_Ships_Registration",
                table: "Ships",
                column: "Registration");

            migrationBuilder.CreateIndex(
                name: "IX_Ships_StlFuelStoreId",
                table: "Ships",
                column: "StlFuelStoreId");

            migrationBuilder.CreateIndex(
                name: "IX_Ships_StoreId",
                table: "Ships",
                column: "StoreId");

            migrationBuilder.CreateIndex(
                name: "IX_SiteBuildings_SiteId",
                table: "SiteBuildings",
                column: "SiteId");

            migrationBuilder.CreateIndex(
                name: "IX_SiteReclaimableMaterials_SiteBuildingId",
                table: "SiteReclaimableMaterials",
                column: "SiteBuildingId");

            migrationBuilder.CreateIndex(
                name: "IX_SiteRepairMaterials_SiteBuildingId",
                table: "SiteRepairMaterials",
                column: "SiteBuildingId");

            migrationBuilder.CreateIndex(
                name: "IX_Sites_PlanetId",
                table: "Sites",
                column: "PlanetId");

            migrationBuilder.CreateIndex(
                name: "IX_Sites_PlanetIdentifier",
                table: "Sites",
                column: "PlanetIdentifier");

            migrationBuilder.CreateIndex(
                name: "IX_Sites_PlanetName",
                table: "Sites",
                column: "PlanetName");

            migrationBuilder.CreateIndex(
                name: "IX_Storages_AddressableId",
                table: "Storages",
                column: "AddressableId");

            migrationBuilder.CreateIndex(
                name: "IX_Systems_Name",
                table: "Systems",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_Systems_NaturalId",
                table: "Systems",
                column: "NaturalId");

            migrationBuilder.CreateIndex(
                name: "IX_UserData_UserName",
                table: "UserData",
                column: "UserName");

            migrationBuilder.CreateIndex(
                name: "IX_UserSettings_UserName",
                table: "UserSettings",
                column: "UserName");

            migrationBuilder.CreateIndex(
                name: "IX_Warehouses_StoreId",
                table: "Warehouses",
                column: "StoreId");

            migrationBuilder.CreateIndex(
                name: "IX_Workforces_PlanetId",
                table: "Workforces",
                column: "PlanetId");

            migrationBuilder.CreateIndex(
                name: "IX_Workforces_PlanetName",
                table: "Workforces",
                column: "PlanetName");

            migrationBuilder.CreateIndex(
                name: "IX_Workforces_PlanetNaturalId",
                table: "Workforces",
                column: "PlanetNaturalId");

            migrationBuilder.CreateIndex(
                name: "IX_Workforces_SiteId",
                table: "Workforces",
                column: "SiteId");

            migrationBuilder.AddForeignKey(
                name: "FK_BuyingAds_LocalMarkets_LocalMarketId",
                table: "BuyingAds",
                column: "LocalMarketId",
                principalTable: "LocalMarkets",
                principalColumn: "LocalMarketId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ContractConditions_Contracts_ContractId",
                table: "ContractConditions",
                column: "ContractId",
                principalTable: "Contracts",
                principalColumn: "ContractId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ContractDependencies_ContractConditions_ContractConditionId",
                table: "ContractDependencies",
                column: "ContractConditionId",
                principalTable: "ContractConditions",
                principalColumn: "ContractConditionId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CXOSTrades_CXOSTradeOrders_CXOSTradeOrderId",
                table: "CXOSTrades",
                column: "CXOSTradeOrderId",
                principalTable: "CXOSTradeOrders",
                principalColumn: "CXOSTradeOrderId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CXPCDataEntries_CXPCData_CXPCDataId",
                table: "CXPCDataEntries",
                column: "CXPCDataId",
                principalTable: "CXPCData",
                principalColumn: "CXPCDataId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DestinationLines_FlightSegments_FlightSegmentId",
                table: "DestinationLines",
                column: "FlightSegmentId",
                principalTable: "FlightSegments",
                principalColumn: "FlightSegmentId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_InfrastructureProjectContributions_InfrastructureProjects_InfrastructureProjectId",
                table: "InfrastructureProjectContributions",
                column: "InfrastructureProjectId",
                principalTable: "InfrastructureProjects",
                principalColumn: "InfrastructureProjectId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_InfrastructureProjectUpgradeCosts_InfrastructureProjects_InfrastructureProjectId",
                table: "InfrastructureProjectUpgradeCosts",
                column: "InfrastructureProjectId",
                principalTable: "InfrastructureProjects",
                principalColumn: "InfrastructureProjectId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_InfrastructureProjectUpkeeps_InfrastructureProjects_InfrastructureProjectId",
                table: "InfrastructureProjectUpkeeps",
                column: "InfrastructureProjectId",
                principalTable: "InfrastructureProjects",
                principalColumn: "InfrastructureProjectId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_InfrastructureReports_Infrastructures_InfrastructureId",
                table: "InfrastructureReports",
                column: "InfrastructureId",
                principalTable: "Infrastructures",
                principalColumn: "InfrastructureId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_JumpCacheRoutes_JumpCache_JumpCacheId",
                table: "JumpCacheRoutes",
                column: "JumpCacheId",
                principalTable: "JumpCache",
                principalColumn: "JumpCacheId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_OriginLines_FlightSegments_FlightSegmentId",
                table: "OriginLines",
                column: "FlightSegmentId",
                principalTable: "FlightSegments",
                principalColumn: "FlightSegmentId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductionLineInputs_ProductionLineOrders_ProductionLineOrderId",
                table: "ProductionLineInputs",
                column: "ProductionLineOrderId",
                principalTable: "ProductionLineOrders",
                principalColumn: "ProductionLineOrderId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductionLineOrders_ProductionLines_ProductionLineId",
                table: "ProductionLineOrders",
                column: "ProductionLineId",
                principalTable: "ProductionLines",
                principalColumn: "ProductionLineId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductionLineOutputs_ProductionLineOrders_ProductionLineOrderId",
                table: "ProductionLineOutputs",
                column: "ProductionLineOrderId",
                principalTable: "ProductionLineOrders",
                principalColumn: "ProductionLineOrderId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SellingAds_LocalMarkets_LocalMarketId",
                table: "SellingAds",
                column: "LocalMarketId",
                principalTable: "LocalMarkets",
                principalColumn: "LocalMarketId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ShippingAds_LocalMarkets_LocalMarketId",
                table: "ShippingAds",
                column: "LocalMarketId",
                principalTable: "LocalMarkets",
                principalColumn: "LocalMarketId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_StorageItems_Storages_StorageId",
                table: "StorageItems",
                column: "StorageId",
                principalTable: "Storages",
                principalColumn: "StorageId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SubSectors_Sectors_SectorId",
                table: "SubSectors",
                column: "SectorId",
                principalTable: "Sectors",
                principalColumn: "SectorId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SubSectorVertices_SubSectors_SubSectorId",
                table: "SubSectorVertices",
                column: "SubSectorId",
                principalTable: "SubSectors",
                principalColumn: "SubSectorId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SystemConnections_Systems_SystemId",
                table: "SystemConnections",
                column: "SystemId",
                principalTable: "Systems",
                principalColumn: "SystemId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserSettingsBurnRateExclusions_UserSettingsBurnRates_UserSettingsBurnRateId",
                table: "UserSettingsBurnRateExclusions",
                column: "UserSettingsBurnRateId",
                principalTable: "UserSettingsBurnRates",
                principalColumn: "UserSettingsBurnRateId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserSettingsBurnRates_UserSettings_UserSettingsId",
                table: "UserSettingsBurnRates",
                column: "UserSettingsId",
                principalTable: "UserSettings",
                principalColumn: "UserSettingsId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_WorkforceDescriptions_Workforces_WorkforceId",
                table: "WorkforceDescriptions",
                column: "WorkforceId",
                principalTable: "Workforces",
                principalColumn: "WorkforceId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_WorkforceNeeds_WorkforceDescriptions_WorkforceDescriptionId",
                table: "WorkforceNeeds",
                column: "WorkforceDescriptionId",
                principalTable: "WorkforceDescriptions",
                principalColumn: "WorkforceDescriptionId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BuyingAds_LocalMarkets_LocalMarketId",
                table: "BuyingAds");

            migrationBuilder.DropForeignKey(
                name: "FK_ContractConditions_Contracts_ContractId",
                table: "ContractConditions");

            migrationBuilder.DropForeignKey(
                name: "FK_ContractDependencies_ContractConditions_ContractConditionId",
                table: "ContractDependencies");

            migrationBuilder.DropForeignKey(
                name: "FK_CXOSTrades_CXOSTradeOrders_CXOSTradeOrderId",
                table: "CXOSTrades");

            migrationBuilder.DropForeignKey(
                name: "FK_CXPCDataEntries_CXPCData_CXPCDataId",
                table: "CXPCDataEntries");

            migrationBuilder.DropForeignKey(
                name: "FK_DestinationLines_FlightSegments_FlightSegmentId",
                table: "DestinationLines");

            migrationBuilder.DropForeignKey(
                name: "FK_InfrastructureProjectContributions_InfrastructureProjects_InfrastructureProjectId",
                table: "InfrastructureProjectContributions");

            migrationBuilder.DropForeignKey(
                name: "FK_InfrastructureProjectUpgradeCosts_InfrastructureProjects_InfrastructureProjectId",
                table: "InfrastructureProjectUpgradeCosts");

            migrationBuilder.DropForeignKey(
                name: "FK_InfrastructureProjectUpkeeps_InfrastructureProjects_InfrastructureProjectId",
                table: "InfrastructureProjectUpkeeps");

            migrationBuilder.DropForeignKey(
                name: "FK_InfrastructureReports_Infrastructures_InfrastructureId",
                table: "InfrastructureReports");

            migrationBuilder.DropForeignKey(
                name: "FK_JumpCacheRoutes_JumpCache_JumpCacheId",
                table: "JumpCacheRoutes");

            migrationBuilder.DropForeignKey(
                name: "FK_OriginLines_FlightSegments_FlightSegmentId",
                table: "OriginLines");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductionLineInputs_ProductionLineOrders_ProductionLineOrderId",
                table: "ProductionLineInputs");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductionLineOrders_ProductionLines_ProductionLineId",
                table: "ProductionLineOrders");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductionLineOutputs_ProductionLineOrders_ProductionLineOrderId",
                table: "ProductionLineOutputs");

            migrationBuilder.DropForeignKey(
                name: "FK_SellingAds_LocalMarkets_LocalMarketId",
                table: "SellingAds");

            migrationBuilder.DropForeignKey(
                name: "FK_ShippingAds_LocalMarkets_LocalMarketId",
                table: "ShippingAds");

            migrationBuilder.DropForeignKey(
                name: "FK_StorageItems_Storages_StorageId",
                table: "StorageItems");

            migrationBuilder.DropForeignKey(
                name: "FK_SubSectors_Sectors_SectorId",
                table: "SubSectors");

            migrationBuilder.DropForeignKey(
                name: "FK_SubSectorVertices_SubSectors_SubSectorId",
                table: "SubSectorVertices");

            migrationBuilder.DropForeignKey(
                name: "FK_SystemConnections_Systems_SystemId",
                table: "SystemConnections");

            migrationBuilder.DropForeignKey(
                name: "FK_UserSettingsBurnRateExclusions_UserSettingsBurnRates_UserSettingsBurnRateId",
                table: "UserSettingsBurnRateExclusions");

            migrationBuilder.DropForeignKey(
                name: "FK_UserSettingsBurnRates_UserSettings_UserSettingsId",
                table: "UserSettingsBurnRates");

            migrationBuilder.DropForeignKey(
                name: "FK_WorkforceDescriptions_Workforces_WorkforceId",
                table: "WorkforceDescriptions");

            migrationBuilder.DropForeignKey(
                name: "FK_WorkforceNeeds_WorkforceDescriptions_WorkforceDescriptionId",
                table: "WorkforceNeeds");

            migrationBuilder.DropTable(
                name: "BuildingCosts");

            migrationBuilder.DropTable(
                name: "BuildingDegradationReclaimableMaterials");

            migrationBuilder.DropTable(
                name: "BuildingDegradationRepairMaterials");

            migrationBuilder.DropTable(
                name: "BuildingRecipeInputs");

            migrationBuilder.DropTable(
                name: "BuildingRecipeOutputs");

            migrationBuilder.DropTable(
                name: "CompanyCurrencyBalances");

            migrationBuilder.DropTable(
                name: "Contracts");

            migrationBuilder.DropTable(
                name: "Experts");

            migrationBuilder.DropTable(
                name: "FlightSegments");

            migrationBuilder.DropTable(
                name: "FXDataPairs");

            migrationBuilder.DropTable(
                name: "InfrastructureProjects");

            migrationBuilder.DropTable(
                name: "JumpCache");

            migrationBuilder.DropTable(
                name: "LocalMarkets");

            migrationBuilder.DropTable(
                name: "Materials");

            migrationBuilder.DropTable(
                name: "Sectors");

            migrationBuilder.DropTable(
                name: "ShipRepairMaterials");

            migrationBuilder.DropTable(
                name: "SiteReclaimableMaterials");

            migrationBuilder.DropTable(
                name: "SiteRepairMaterials");

            migrationBuilder.DropTable(
                name: "Storages");

            migrationBuilder.DropTable(
                name: "Systems");

            migrationBuilder.DropTable(
                name: "UserData");

            migrationBuilder.DropTable(
                name: "UserSettings");

            migrationBuilder.DropTable(
                name: "Warehouses");

            migrationBuilder.DropTable(
                name: "Workforces");

            migrationBuilder.DropTable(
                name: "BuildingDegradations");

            migrationBuilder.DropTable(
                name: "BuildingRecipes");

            migrationBuilder.DropTable(
                name: "Companies");

            migrationBuilder.DropTable(
                name: "Flights");

            migrationBuilder.DropTable(
                name: "Infrastructures");

            migrationBuilder.DropTable(
                name: "Ships");

            migrationBuilder.DropTable(
                name: "SiteBuildings");

            migrationBuilder.DropTable(
                name: "Buildings");

            migrationBuilder.DropTable(
                name: "Sites");

            migrationBuilder.DropIndex(
                name: "IX_WorkforceDescriptions_WorkforceId",
                table: "WorkforceDescriptions");

            migrationBuilder.DropIndex(
                name: "IX_UserSettingsBurnRates_UserSettingsId",
                table: "UserSettingsBurnRates");

            migrationBuilder.DropIndex(
                name: "IX_SystemStars_SystemId",
                table: "SystemStars");

            migrationBuilder.DropIndex(
                name: "IX_SystemStars_SystemName",
                table: "SystemStars");

            migrationBuilder.DropIndex(
                name: "IX_SystemStars_SystemNaturalId",
                table: "SystemStars");

            migrationBuilder.DropIndex(
                name: "IX_SystemConnections_SystemId",
                table: "SystemConnections");

            migrationBuilder.DropIndex(
                name: "IX_SubSectors_SectorId",
                table: "SubSectors");

            migrationBuilder.DropIndex(
                name: "IX_StorageItems_StorageId",
                table: "StorageItems");

            migrationBuilder.DropIndex(
                name: "IX_ShippingAds_LocalMarketId",
                table: "ShippingAds");

            migrationBuilder.DropIndex(
                name: "IX_ShippingAds_PlanetId",
                table: "ShippingAds");

            migrationBuilder.DropIndex(
                name: "IX_ShippingAds_PlanetName",
                table: "ShippingAds");

            migrationBuilder.DropIndex(
                name: "IX_ShippingAds_PlanetNaturalId",
                table: "ShippingAds");

            migrationBuilder.DropIndex(
                name: "IX_SellingAds_LocalMarketId",
                table: "SellingAds");

            migrationBuilder.DropIndex(
                name: "IX_SellingAds_PlanetId",
                table: "SellingAds");

            migrationBuilder.DropIndex(
                name: "IX_SellingAds_PlanetName",
                table: "SellingAds");

            migrationBuilder.DropIndex(
                name: "IX_SellingAds_PlanetNaturalId",
                table: "SellingAds");

            migrationBuilder.DropIndex(
                name: "IX_ProductionLines_PlanetId",
                table: "ProductionLines");

            migrationBuilder.DropIndex(
                name: "IX_ProductionLines_PlanetName",
                table: "ProductionLines");

            migrationBuilder.DropIndex(
                name: "IX_ProductionLines_PlanetNaturalId",
                table: "ProductionLines");

            migrationBuilder.DropIndex(
                name: "IX_ProductionLines_SiteId",
                table: "ProductionLines");

            migrationBuilder.DropIndex(
                name: "IX_JumpCacheRoutes_DestinationSystemId",
                table: "JumpCacheRoutes");

            migrationBuilder.DropIndex(
                name: "IX_JumpCacheRoutes_JumpCacheId",
                table: "JumpCacheRoutes");

            migrationBuilder.DropIndex(
                name: "IX_JumpCacheRoutes_SourceSystemId",
                table: "JumpCacheRoutes");

            migrationBuilder.DropIndex(
                name: "IX_InfrastructureProjectUpkeeps_InfrastructureProjectId",
                table: "InfrastructureProjectUpkeeps");

            migrationBuilder.DropIndex(
                name: "IX_InfrastructureProjectUpgradeCosts_InfrastructureProjectId",
                table: "InfrastructureProjectUpgradeCosts");

            migrationBuilder.DropIndex(
                name: "IX_InfrastructureProjectContributions_InfrastructureProjectId",
                table: "InfrastructureProjectContributions");

            migrationBuilder.DropIndex(
                name: "IX_ContractConditions_ContractId",
                table: "ContractConditions");

            migrationBuilder.DropIndex(
                name: "IX_BuyingAds_LocalMarketId",
                table: "BuyingAds");

            migrationBuilder.DropIndex(
                name: "IX_BuyingAds_PlanetId",
                table: "BuyingAds");

            migrationBuilder.DropIndex(
                name: "IX_BuyingAds_PlanetName",
                table: "BuyingAds");

            migrationBuilder.DropIndex(
                name: "IX_BuyingAds_PlanetNaturalId",
                table: "BuyingAds");

            migrationBuilder.DropPrimaryKey(
                name: "PK_OriginLines",
                table: "OriginLines");

            migrationBuilder.DropIndex(
                name: "IX_OriginLines_FlightSegmentId",
                table: "OriginLines");

            migrationBuilder.DropPrimaryKey(
                name: "PK_InfrastructureReports",
                table: "InfrastructureReports");

            migrationBuilder.DropIndex(
                name: "IX_InfrastructureReports_InfrastructureId",
                table: "InfrastructureReports");

            migrationBuilder.DropPrimaryKey(
                name: "PK_DestinationLines",
                table: "DestinationLines");

            migrationBuilder.DropIndex(
                name: "IX_DestinationLines_FlightSegmentId",
                table: "DestinationLines");

            migrationBuilder.DropColumn(
                name: "WorkforceId",
                table: "WorkforceDescriptions");

            migrationBuilder.DropColumn(
                name: "UserSettingsId",
                table: "UserSettingsBurnRates");

            migrationBuilder.DropColumn(
                name: "StorageId",
                table: "StorageItems");

            migrationBuilder.DropColumn(
                name: "LocalMarketId",
                table: "ShippingAds");

            migrationBuilder.DropColumn(
                name: "LocalMarketId",
                table: "SellingAds");

            migrationBuilder.DropColumn(
                name: "Timestamp",
                table: "ProductionLines");

            migrationBuilder.DropColumn(
                name: "JumpCacheId",
                table: "JumpCacheRoutes");

            migrationBuilder.DropColumn(
                name: "InfrastructureProjectId",
                table: "InfrastructureProjectUpkeeps");

            migrationBuilder.DropColumn(
                name: "InfrastructureProjectId",
                table: "InfrastructureProjectUpgradeCosts");

            migrationBuilder.DropColumn(
                name: "InfrastructureProjectId",
                table: "InfrastructureProjectContributions");

            migrationBuilder.DropColumn(
                name: "Timestamp",
                table: "CXPCData");

            migrationBuilder.DropColumn(
                name: "UserNameSubmitted",
                table: "CXPCData");

            migrationBuilder.DropColumn(
                name: "Timestamp",
                table: "CXOSTradeOrders");

            migrationBuilder.DropColumn(
                name: "ContractId",
                table: "ContractConditions");

            migrationBuilder.DropColumn(
                name: "LocalMarketId",
                table: "BuyingAds");

            migrationBuilder.DropColumn(
                name: "FlightSegmentId",
                table: "OriginLines");

            migrationBuilder.DropColumn(
                name: "InfrastructureId",
                table: "InfrastructureReports");

            migrationBuilder.DropColumn(
                name: "FlightSegmentId",
                table: "DestinationLines");

            migrationBuilder.RenameTable(
                name: "OriginLines",
                newName: "FLIGHTSOriginLines");

            migrationBuilder.RenameTable(
                name: "InfrastructureReports",
                newName: "InfrastructureModelReports");

            migrationBuilder.RenameTable(
                name: "DestinationLines",
                newName: "FLIGHTSDestinationLines");

            migrationBuilder.RenameColumn(
                name: "SystemId",
                table: "SystemConnections",
                newName: "Connection");

            migrationBuilder.RenameColumn(
                name: "SectorId",
                table: "SubSectors",
                newName: "SSId");

            migrationBuilder.RenameColumn(
                name: "UserNameSubmitted",
                table: "ProductionLines",
                newName: "LineId");

            migrationBuilder.RenameColumn(
                name: "UserNameSubmitted",
                table: "CXOSTradeOrders",
                newName: "TradeOrderId");

            migrationBuilder.AlterColumn<int>(
                name: "WorkforceDescriptionId",
                table: "WorkforceNeeds",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "WorkforceNeedId",
                table: "WorkforceNeeds",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldMaxLength: 60)
                .Annotation("Sqlite:Autoincrement", true);

            migrationBuilder.AlterColumn<int>(
                name: "WorkforceDescriptionId",
                table: "WorkforceDescriptions",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldMaxLength: 50)
                .Annotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddColumn<int>(
                name: "WorkforceModelId",
                table: "WorkforceDescriptions",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "UserSettingsBurnRateId",
                table: "UserSettingsBurnRates",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldMaxLength: 41)
                .Annotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddColumn<int>(
                name: "UserSettingsModelId",
                table: "UserSettingsBurnRates",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "UserSettingsBurnRateId",
                table: "UserSettingsBurnRateExclusions",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldMaxLength: 41,
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "UserSettingsBurnRateExclusionId",
                table: "UserSettingsBurnRateExclusions",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldMaxLength: 50)
                .Annotation("Sqlite:Autoincrement", true);

            migrationBuilder.AlterColumn<int>(
                name: "SystemStarId",
                table: "SystemStars",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldMaxLength: 32)
                .Annotation("Sqlite:Autoincrement", true);

            migrationBuilder.AlterColumn<int>(
                name: "SystemConnectionId",
                table: "SystemConnections",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldMaxLength: 32)
                .Annotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddColumn<int>(
                name: "SystemStarsModelId",
                table: "SystemConnections",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "SubSectorId",
                table: "SubSectorVertices",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldMaxLength: 32,
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "SubSectorVertexId",
                table: "SubSectorVertices",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldMaxLength: 50)
                .Annotation("Sqlite:Autoincrement", true);

            migrationBuilder.AlterColumn<int>(
                name: "SubSectorId",
                table: "SubSectors",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldMaxLength: 32)
                .Annotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddColumn<int>(
                name: "WorldSectorsModelId",
                table: "SubSectors",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "StorageItemId",
                table: "StorageItems",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldMaxLength: 65)
                .Annotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddColumn<int>(
                name: "StorageModelId",
                table: "StorageItems",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "StationId",
                table: "Stations",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldMaxLength: 32)
                .Annotation("Sqlite:Autoincrement", true);

            migrationBuilder.AlterColumn<int>(
                name: "ShippingAdId",
                table: "ShippingAds",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldMaxLength: 64)
                .Annotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddColumn<int>(
                name: "LocalMarketModelId",
                table: "ShippingAds",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "SellingAdId",
                table: "SellingAds",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldMaxLength: 64)
                .Annotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddColumn<int>(
                name: "LocalMarketModelId",
                table: "SellingAds",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "ProductionLineId",
                table: "ProductionLines",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldMaxLength: 32)
                .Annotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddColumn<int>(
                name: "PRODLinesModelId",
                table: "ProductionLines",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "ProductionLineOrderId",
                table: "ProductionLineOutputs",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldMaxLength: 32,
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ProductionLineOutputId",
                table: "ProductionLineOutputs",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldMaxLength: 41)
                .Annotation("Sqlite:Autoincrement", true);

            migrationBuilder.AlterColumn<int>(
                name: "ProductionLineId",
                table: "ProductionLineOrders",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldMaxLength: 32,
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ProductionLineOrderId",
                table: "ProductionLineOrders",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldMaxLength: 32)
                .Annotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddColumn<string>(
                name: "ProductionId",
                table: "ProductionLineOrders",
                type: "TEXT",
                maxLength: 32,
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ProductionLineOrderId",
                table: "ProductionLineInputs",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ProductionLineInputId",
                table: "ProductionLineInputs",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldMaxLength: 41)
                .Annotation("Sqlite:Autoincrement", true);

            migrationBuilder.AlterColumn<int>(
                name: "JumpCacheRouteJumpId",
                table: "JumpCacheRoutes",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldMaxLength: 131)
                .Annotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddColumn<int>(
                name: "JumpCacheModelId",
                table: "JumpCacheRoutes",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "InfrastructureProjectUpkeepsId",
                table: "InfrastructureProjectUpkeeps",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldMaxLength: 128)
                .Annotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddColumn<int>(
                name: "InfrastructureInfoId",
                table: "InfrastructureProjectUpkeeps",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "InfrastructureProjectUpgradeCostsId",
                table: "InfrastructureProjectUpgradeCosts",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldMaxLength: 128)
                .Annotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddColumn<int>(
                name: "InfrastructureInfoId",
                table: "InfrastructureProjectUpgradeCosts",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "InfrastructureProjectContributionsId",
                table: "InfrastructureProjectContributions",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldMaxLength: 128)
                .Annotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddColumn<int>(
                name: "InfrastructureInfoId",
                table: "InfrastructureProjectContributions",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "CXPCDataId",
                table: "CXPCDataEntries",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldMaxLength: 32,
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CXPCDataEntryId",
                table: "CXPCDataEntries",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldMaxLength: 64)
                .Annotation("Sqlite:Autoincrement", true);

            migrationBuilder.AlterColumn<int>(
                name: "CXPCDataId",
                table: "CXPCData",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldMaxLength: 32)
                .Annotation("Sqlite:Autoincrement", true);

            migrationBuilder.AlterColumn<int>(
                name: "CXOSTradeOrderId",
                table: "CXOSTrades",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldMaxLength: 32,
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CXOSTradeId",
                table: "CXOSTrades",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldMaxLength: 32)
                .Annotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddColumn<string>(
                name: "TradeId",
                table: "CXOSTrades",
                type: "TEXT",
                maxLength: 32,
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CXOSTradeOrderId",
                table: "CXOSTradeOrders",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldMaxLength: 32)
                .Annotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddColumn<int>(
                name: "CXOSTradeOrdersModelId",
                table: "CXOSTradeOrders",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "CountryRegistryCountryId",
                table: "CountryRegistryCountries",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldMaxLength: 32)
                .Annotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddColumn<string>(
                name: "CountryId",
                table: "CountryRegistryCountries",
                type: "TEXT",
                maxLength: 32,
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ContractConditionId",
                table: "ContractDependencies",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldMaxLength: 40,
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ContractDependencyId",
                table: "ContractDependencies",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldMaxLength: 80)
                .Annotation("Sqlite:Autoincrement", true);

            migrationBuilder.AlterColumn<int>(
                name: "ContractConditionId",
                table: "ContractConditions",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldMaxLength: 40)
                .Annotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddColumn<int>(
                name: "ContractModelId",
                table: "ContractConditions",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "ComexExchangeId",
                table: "ComexExchanges",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldMaxLength: 32)
                .Annotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddColumn<string>(
                name: "ExchangeId",
                table: "ComexExchanges",
                type: "TEXT",
                maxLength: 32,
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "BuyingAdId",
                table: "BuyingAds",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldMaxLength: 64)
                .Annotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddColumn<int>(
                name: "LocalMarketModelId",
                table: "BuyingAds",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "OriginLineId",
                table: "FLIGHTSOriginLines",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldMaxLength: 128)
                .Annotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddColumn<int>(
                name: "FLIGHTSFlightSegmentId",
                table: "FLIGHTSOriginLines",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "InfrastructureReportId",
                table: "InfrastructureModelReports",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldMaxLength: 40)
                .Annotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddColumn<int>(
                name: "InfrastructureModelId",
                table: "InfrastructureModelReports",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "DestinationLineId",
                table: "FLIGHTSDestinationLines",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldMaxLength: 128)
                .Annotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddColumn<int>(
                name: "FLIGHTSFlightSegmentId",
                table: "FLIGHTSDestinationLines",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_FLIGHTSOriginLines",
                table: "FLIGHTSOriginLines",
                column: "OriginLineId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_InfrastructureModelReports",
                table: "InfrastructureModelReports",
                column: "InfrastructureReportId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_FLIGHTSDestinationLines",
                table: "FLIGHTSDestinationLines",
                column: "DestinationLineId");

            migrationBuilder.CreateTable(
                name: "BuildingDegradationModels",
                columns: table => new
                {
                    BuildingDegradationModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    BuildingCreated = table.Column<long>(type: "INTEGER", nullable: false),
                    BuildingTicker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    Condition = table.Column<double>(type: "REAL", nullable: false),
                    DaysSinceLastRepair = table.Column<double>(type: "REAL", nullable: false),
                    HasBeenRepaired = table.Column<bool>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuildingDegradationModels", x => x.BuildingDegradationModelId);
                });

            migrationBuilder.CreateTable(
                name: "BUIModels",
                columns: table => new
                {
                    BUIModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    AreaCost = table.Column<int>(type: "INTEGER", nullable: false),
                    Engineers = table.Column<int>(type: "INTEGER", nullable: false),
                    Expertise = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Name = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    Pioneers = table.Column<int>(type: "INTEGER", nullable: false),
                    Scientists = table.Column<int>(type: "INTEGER", nullable: false),
                    Settlers = table.Column<int>(type: "INTEGER", nullable: false),
                    Technicians = table.Column<int>(type: "INTEGER", nullable: false),
                    Ticker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BUIModels", x => x.BUIModelId);
                });

            migrationBuilder.CreateTable(
                name: "CompanyDataModels",
                columns: table => new
                {
                    CompanyDataModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ActivityRating = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    CompanyCode = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    CompanyId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    CompanyName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    CountryId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    CreatedEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    CurrencyCode = table.Column<string>(type: "TEXT", maxLength: 4, nullable: true),
                    HighestTier = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    OverallRating = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    Pioneer = table.Column<bool>(type: "INTEGER", nullable: false),
                    ReliabilityRating = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    StabilityRating = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    StartingLocation = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    StartingProfile = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Team = table.Column<bool>(type: "INTEGER", nullable: false),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false),
                    UserName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompanyDataModels", x => x.CompanyDataModelId);
                });

            migrationBuilder.CreateTable(
                name: "ContractModels",
                columns: table => new
                {
                    ContractModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CanExtend = table.Column<bool>(type: "INTEGER", nullable: false),
                    ContractId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    ContractLocalId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    DateEpochMs = table.Column<long>(type: "INTEGER", nullable: true),
                    DueDateEpochMs = table.Column<long>(type: "INTEGER", nullable: true),
                    ExtensionDeadlineEpochMs = table.Column<long>(type: "INTEGER", nullable: true),
                    PartnerCompanyCode = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    PartnerId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    PartnerName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    Party = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Status = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContractModels", x => x.ContractModelId);
                });

            migrationBuilder.CreateTable(
                name: "CXOSTradeOrderModels",
                columns: table => new
                {
                    CXOSTradeOrdersModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CXOSTradeOrderModels", x => x.CXOSTradeOrdersModelId);
                });

            migrationBuilder.CreateTable(
                name: "ExpertModels",
                columns: table => new
                {
                    ExpertsModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    AgricultureActive = table.Column<int>(type: "INTEGER", nullable: false),
                    AgricultureAvailable = table.Column<int>(type: "INTEGER", nullable: false),
                    AgricultureEfficiencyGain = table.Column<double>(type: "REAL", nullable: false),
                    ChemistryActive = table.Column<int>(type: "INTEGER", nullable: false),
                    ChemistryAvailable = table.Column<int>(type: "INTEGER", nullable: false),
                    ChemistryEfficiencyGain = table.Column<double>(type: "REAL", nullable: false),
                    ConstructionActive = table.Column<int>(type: "INTEGER", nullable: false),
                    ConstructionAvailable = table.Column<int>(type: "INTEGER", nullable: false),
                    ConstructionEfficiencyGain = table.Column<double>(type: "REAL", nullable: false),
                    ElectronicsActive = table.Column<int>(type: "INTEGER", nullable: false),
                    ElectronicsAvailable = table.Column<int>(type: "INTEGER", nullable: false),
                    ElectronicsEfficiencyGain = table.Column<double>(type: "REAL", nullable: false),
                    FoodIndustriesActive = table.Column<int>(type: "INTEGER", nullable: false),
                    FoodIndustriesAvailable = table.Column<int>(type: "INTEGER", nullable: false),
                    FoodIndustriesEfficiencyGain = table.Column<double>(type: "REAL", nullable: false),
                    FuelRefiningActive = table.Column<int>(type: "INTEGER", nullable: false),
                    FuelRefiningAvailable = table.Column<int>(type: "INTEGER", nullable: false),
                    FuelRefiningEfficiencyGain = table.Column<double>(type: "REAL", nullable: false),
                    ManufacturingActive = table.Column<int>(type: "INTEGER", nullable: false),
                    ManufacturingAvailable = table.Column<int>(type: "INTEGER", nullable: false),
                    ManufacturingEfficiencyGain = table.Column<double>(type: "REAL", nullable: false),
                    MetallurgyActive = table.Column<int>(type: "INTEGER", nullable: false),
                    MetallurgyAvailable = table.Column<int>(type: "INTEGER", nullable: false),
                    MetallurgyEfficiencyGain = table.Column<double>(type: "REAL", nullable: false),
                    PlanetId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    ResourceExtractionAvailable = table.Column<int>(type: "INTEGER", nullable: false),
                    ResourceExtractionEfficiencyGain = table.Column<double>(type: "REAL", nullable: false),
                    ResourceExtractioneActive = table.Column<int>(type: "INTEGER", nullable: false),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExpertModels", x => x.ExpertsModelId);
                });

            migrationBuilder.CreateTable(
                name: "FLIGHTSModels",
                columns: table => new
                {
                    FLIGHTSModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FLIGHTSModels", x => x.FLIGHTSModelId);
                });

            migrationBuilder.CreateTable(
                name: "FXDataModels",
                columns: table => new
                {
                    FXDataModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    BaseCurrencyCode = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    BaseCurrencyName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    BaseCurrencyNumericCode = table.Column<int>(type: "INTEGER", nullable: false),
                    BrokerId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    High = table.Column<decimal>(type: "TEXT", nullable: false),
                    Low = table.Column<decimal>(type: "TEXT", nullable: false),
                    Open = table.Column<decimal>(type: "TEXT", nullable: false),
                    Previous = table.Column<decimal>(type: "TEXT", nullable: false),
                    PriceUpdateEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    QuoteCurrencyCode = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    QuoteCurrencyName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    QuoteCurrencyNumericCode = table.Column<int>(type: "INTEGER", nullable: false),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false),
                    Traded = table.Column<decimal>(type: "TEXT", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Volume = table.Column<decimal>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FXDataModels", x => x.FXDataModelId);
                });

            migrationBuilder.CreateTable(
                name: "InfrastructureModels",
                columns: table => new
                {
                    InfrastructureModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    PopulationId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InfrastructureModels", x => x.InfrastructureModelId);
                });

            migrationBuilder.CreateTable(
                name: "JumpCacheModels",
                columns: table => new
                {
                    JumpCacheModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    DestinationNaturalId = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    DestinationSystemId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    DestinationSystemName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    JumpCount = table.Column<int>(type: "INTEGER", nullable: false),
                    OverallDistance = table.Column<double>(type: "REAL", nullable: false),
                    SourceSystemId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    SourceSystemName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    SourceSystemNaturalId = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JumpCacheModels", x => x.JumpCacheModelId);
                });

            migrationBuilder.CreateTable(
                name: "LocalMarketModels",
                columns: table => new
                {
                    LocalMarketModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    MarketId = table.Column<string>(type: "TEXT", nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LocalMarketModels", x => x.LocalMarketModelId);
                });

            migrationBuilder.CreateTable(
                name: "MATModels",
                columns: table => new
                {
                    MATModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CategoryId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    CategoryName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    MatId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Name = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    Ticker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Volume = table.Column<double>(type: "REAL", nullable: false),
                    Weight = table.Column<double>(type: "REAL", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MATModels", x => x.MATModelId);
                });

            migrationBuilder.CreateTable(
                name: "PRODLinesModels",
                columns: table => new
                {
                    PRODLinesModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    SiteId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PRODLinesModels", x => x.PRODLinesModelId);
                });

            migrationBuilder.CreateTable(
                name: "SHIPSModels",
                columns: table => new
                {
                    SHIPSModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SHIPSModels", x => x.SHIPSModelId);
                });

            migrationBuilder.CreateTable(
                name: "SITESModels",
                columns: table => new
                {
                    SITESModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SITESModels", x => x.SITESModelId);
                });

            migrationBuilder.CreateTable(
                name: "StorageModels",
                columns: table => new
                {
                    StorageModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    AddressableId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    FixedStore = table.Column<bool>(type: "INTEGER", nullable: false),
                    Name = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    StorageId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false),
                    Type = table.Column<string>(type: "TEXT", maxLength: 16, nullable: true),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    VolumeCapacity = table.Column<double>(type: "REAL", nullable: false),
                    VolumeLoad = table.Column<double>(type: "REAL", nullable: false),
                    WeightCapacity = table.Column<double>(type: "REAL", nullable: false),
                    WeightLoad = table.Column<double>(type: "REAL", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StorageModels", x => x.StorageModelId);
                });

            migrationBuilder.CreateTable(
                name: "SystemStarsModels",
                columns: table => new
                {
                    SystemStarsModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    NaturalId = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    PositionX = table.Column<double>(type: "REAL", nullable: false),
                    PositionY = table.Column<double>(type: "REAL", nullable: false),
                    PositionZ = table.Column<double>(type: "REAL", nullable: false),
                    SectorId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    SubSectorId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    SystemId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false),
                    Type = table.Column<string>(type: "TEXT", maxLength: 16, nullable: true),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SystemStarsModels", x => x.SystemStarsModelId);
                });

            migrationBuilder.CreateTable(
                name: "UserDataModels",
                columns: table => new
                {
                    UserDataModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CompanyId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    CreatedEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    IsModeratorChat = table.Column<bool>(type: "INTEGER", nullable: false),
                    IsPayingUser = table.Column<bool>(type: "INTEGER", nullable: false),
                    Pioneer = table.Column<bool>(type: "INTEGER", nullable: false),
                    PlanetNamingRights = table.Column<int>(type: "INTEGER", nullable: false),
                    SystemNamingRights = table.Column<int>(type: "INTEGER", nullable: false),
                    Team = table.Column<bool>(type: "INTEGER", nullable: false),
                    Tier = table.Column<string>(type: "TEXT", maxLength: 16, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false),
                    UserName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserDataModels", x => x.UserDataModelId);
                });

            migrationBuilder.CreateTable(
                name: "UserSettingsModels",
                columns: table => new
                {
                    UserSettingsModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    UserName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserSettingsModels", x => x.UserSettingsModelId);
                });

            migrationBuilder.CreateTable(
                name: "WarehouseModels",
                columns: table => new
                {
                    WarehouseModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    FeeAmount = table.Column<double>(type: "REAL", nullable: false),
                    FeeCollectorCode = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    FeeCollectorId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    FeeCollectorName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    FeeCurrency = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    LocationName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    LocationNaturalId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    NextPaymentTimestampEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    StoreId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false),
                    Units = table.Column<int>(type: "INTEGER", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    VolumeCapacity = table.Column<double>(type: "REAL", nullable: false),
                    WarehouseId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    WeightCapacity = table.Column<double>(type: "REAL", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WarehouseModels", x => x.WarehouseModelId);
                });

            migrationBuilder.CreateTable(
                name: "WorkforceModels",
                columns: table => new
                {
                    WorkforceModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    LastWorkforceUpdateTime = table.Column<DateTime>(type: "TEXT", nullable: true),
                    PlanetId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    PlanetName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    PlanetNaturalId = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    SiteId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkforceModels", x => x.WorkforceModelId);
                });

            migrationBuilder.CreateTable(
                name: "WorldSectorsModels",
                columns: table => new
                {
                    WorldSectorsModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    HexQ = table.Column<int>(type: "INTEGER", nullable: false),
                    HexR = table.Column<int>(type: "INTEGER", nullable: false),
                    HexS = table.Column<int>(type: "INTEGER", nullable: false),
                    Name = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    SectorId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Size = table.Column<int>(type: "INTEGER", nullable: false),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorldSectorsModels", x => x.WorldSectorsModelId);
                });

            migrationBuilder.CreateTable(
                name: "BuildingDegradationReclaimableMaterialModels",
                columns: table => new
                {
                    BuildingDegradationReclaimableMaterialModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    BuildingDegradationModelId = table.Column<int>(type: "INTEGER", nullable: false),
                    MaterialCount = table.Column<int>(type: "INTEGER", nullable: false),
                    MaterialTicker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuildingDegradationReclaimableMaterialModels", x => x.BuildingDegradationReclaimableMaterialModelId);
                    table.ForeignKey(
                        name: "FK_BuildingDegradationReclaimableMaterialModels_BuildingDegradationModels_BuildingDegradationModelId",
                        column: x => x.BuildingDegradationModelId,
                        principalTable: "BuildingDegradationModels",
                        principalColumn: "BuildingDegradationModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BuildingDegradationRepairMaterialModels",
                columns: table => new
                {
                    BuildingDegradationRepairMaterialModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    BuildingDegradationModelId = table.Column<int>(type: "INTEGER", nullable: false),
                    MaterialCount = table.Column<int>(type: "INTEGER", nullable: false),
                    MaterialTicker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuildingDegradationRepairMaterialModels", x => x.BuildingDegradationRepairMaterialModelId);
                    table.ForeignKey(
                        name: "FK_BuildingDegradationRepairMaterialModels_BuildingDegradationModels_BuildingDegradationModelId",
                        column: x => x.BuildingDegradationModelId,
                        principalTable: "BuildingDegradationModels",
                        principalColumn: "BuildingDegradationModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BUIBuildingCosts",
                columns: table => new
                {
                    BUIBuildingCostId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Amount = table.Column<int>(type: "INTEGER", nullable: false),
                    BUIModelId = table.Column<int>(type: "INTEGER", nullable: false),
                    CommodityName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    CommodityTicker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    Volume = table.Column<double>(type: "REAL", nullable: false),
                    Weight = table.Column<double>(type: "REAL", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BUIBuildingCosts", x => x.BUIBuildingCostId);
                    table.ForeignKey(
                        name: "FK_BUIBuildingCosts_BUIModels_BUIModelId",
                        column: x => x.BUIModelId,
                        principalTable: "BUIModels",
                        principalColumn: "BUIModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BUIRecipes",
                columns: table => new
                {
                    BUIRecipeId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    BUIModelId = table.Column<int>(type: "INTEGER", nullable: false),
                    DurationMs = table.Column<int>(type: "INTEGER", nullable: false),
                    RecipeName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BUIRecipes", x => x.BUIRecipeId);
                    table.ForeignKey(
                        name: "FK_BUIRecipes_BUIModels_BUIModelId",
                        column: x => x.BUIModelId,
                        principalTable: "BUIModels",
                        principalColumn: "BUIModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CompanyDataCurrencyBalances",
                columns: table => new
                {
                    CompanyDataCurrencyBalanceId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Balance = table.Column<double>(type: "REAL", nullable: false),
                    CompanyDataModelId = table.Column<int>(type: "INTEGER", nullable: false),
                    Currency = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompanyDataCurrencyBalances", x => x.CompanyDataCurrencyBalanceId);
                    table.ForeignKey(
                        name: "FK_CompanyDataCurrencyBalances_CompanyDataModels_CompanyDataModelId",
                        column: x => x.CompanyDataModelId,
                        principalTable: "CompanyDataModels",
                        principalColumn: "CompanyDataModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FLIGHTSFlights",
                columns: table => new
                {
                    FLIGHTSFlightId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ArrivalTimeEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    CurrentSegmentIndex = table.Column<int>(type: "INTEGER", nullable: false),
                    DepartureTimeEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    Destination = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    FLIGHTSModelId = table.Column<int>(type: "INTEGER", nullable: false),
                    FlightId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    FtlDistance = table.Column<double>(type: "REAL", nullable: false),
                    IsAborted = table.Column<bool>(type: "INTEGER", nullable: false),
                    Origin = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    ShipId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    StlDistance = table.Column<double>(type: "REAL", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FLIGHTSFlights", x => x.FLIGHTSFlightId);
                    table.ForeignKey(
                        name: "FK_FLIGHTSFlights_FLIGHTSModels_FLIGHTSModelId",
                        column: x => x.FLIGHTSModelId,
                        principalTable: "FLIGHTSModels",
                        principalColumn: "FLIGHTSModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "InfrastructureInfos",
                columns: table => new
                {
                    InfrastructureInfoId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ActiveLevel = table.Column<int>(type: "INTEGER", nullable: false),
                    CurrentLevel = table.Column<int>(type: "INTEGER", nullable: false),
                    InfrastructureModelId = table.Column<int>(type: "INTEGER", nullable: false),
                    Level = table.Column<int>(type: "INTEGER", nullable: false),
                    Name = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    ProjectId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Ticker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    Type = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    UpgradeStatus = table.Column<double>(type: "REAL", nullable: false),
                    UpkeepStatus = table.Column<double>(type: "REAL", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InfrastructureInfos", x => x.InfrastructureInfoId);
                    table.ForeignKey(
                        name: "FK_InfrastructureInfos_InfrastructureModels_InfrastructureModelId",
                        column: x => x.InfrastructureModelId,
                        principalTable: "InfrastructureModels",
                        principalColumn: "InfrastructureModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SHIPSShips",
                columns: table => new
                {
                    SHIPSShipId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Acceleration = table.Column<double>(type: "REAL", nullable: false),
                    BlueprintNaturalId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    CommissioningTimeEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    Condition = table.Column<double>(type: "REAL", nullable: false),
                    EmitterPower = table.Column<double>(type: "REAL", nullable: false),
                    FlightId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    FtlFuelStoreId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    LastRepairEpochMs = table.Column<long>(type: "INTEGER", nullable: true),
                    Location = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    Mass = table.Column<double>(type: "REAL", nullable: false),
                    Name = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    OperatingEmptyMass = table.Column<double>(type: "REAL", nullable: false),
                    ReactorPower = table.Column<double>(type: "REAL", nullable: false),
                    Registration = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    SHIPSModelId = table.Column<int>(type: "INTEGER", nullable: false),
                    ShipId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    StlFuelFlowRate = table.Column<double>(type: "REAL", nullable: false),
                    StlFuelStoreId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    StoreId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    Thrust = table.Column<double>(type: "REAL", nullable: false),
                    Volume = table.Column<double>(type: "REAL", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SHIPSShips", x => x.SHIPSShipId);
                    table.ForeignKey(
                        name: "FK_SHIPSShips_SHIPSModels_SHIPSModelId",
                        column: x => x.SHIPSModelId,
                        principalTable: "SHIPSModels",
                        principalColumn: "SHIPSModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SITESSites",
                columns: table => new
                {
                    SITESSiteId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    PlanetFoundedEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    PlanetId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    PlanetIdentifier = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    PlanetName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    SITESModelId = table.Column<int>(type: "INTEGER", nullable: false),
                    SiteId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SITESSites", x => x.SITESSiteId);
                    table.ForeignKey(
                        name: "FK_SITESSites_SITESModels_SITESModelId",
                        column: x => x.SITESModelId,
                        principalTable: "SITESModels",
                        principalColumn: "SITESModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BUIRecipeInputs",
                columns: table => new
                {
                    BUIRecipeInputId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Amount = table.Column<int>(type: "INTEGER", nullable: false),
                    BUIRecipeId = table.Column<int>(type: "INTEGER", nullable: false),
                    CommodityName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    CommodityTicker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    Volume = table.Column<double>(type: "REAL", nullable: false),
                    Weight = table.Column<double>(type: "REAL", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BUIRecipeInputs", x => x.BUIRecipeInputId);
                    table.ForeignKey(
                        name: "FK_BUIRecipeInputs_BUIRecipes_BUIRecipeId",
                        column: x => x.BUIRecipeId,
                        principalTable: "BUIRecipes",
                        principalColumn: "BUIRecipeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BUIRecipeOutputs",
                columns: table => new
                {
                    BUIRecipeOutputId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Amount = table.Column<int>(type: "INTEGER", nullable: false),
                    BUIRecipeId = table.Column<int>(type: "INTEGER", nullable: false),
                    CommodityName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    CommodityTicker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    Volume = table.Column<double>(type: "REAL", nullable: false),
                    Weight = table.Column<double>(type: "REAL", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BUIRecipeOutputs", x => x.BUIRecipeOutputId);
                    table.ForeignKey(
                        name: "FK_BUIRecipeOutputs_BUIRecipes_BUIRecipeId",
                        column: x => x.BUIRecipeId,
                        principalTable: "BUIRecipes",
                        principalColumn: "BUIRecipeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FLIGHTSFlightSegments",
                columns: table => new
                {
                    FLIGHTSFlightSegmentId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ArrivalTimeEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    DepartureTimeEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    Destination = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    FLIGHTSFlightId = table.Column<int>(type: "INTEGER", nullable: false),
                    FtlDistance = table.Column<double>(type: "REAL", nullable: true),
                    FtlFuelConsumption = table.Column<double>(type: "REAL", nullable: true),
                    Origin = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    StlDistance = table.Column<double>(type: "REAL", nullable: true),
                    StlFuelConsumption = table.Column<double>(type: "REAL", nullable: true),
                    Type = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FLIGHTSFlightSegments", x => x.FLIGHTSFlightSegmentId);
                    table.ForeignKey(
                        name: "FK_FLIGHTSFlightSegments_FLIGHTSFlights_FLIGHTSFlightId",
                        column: x => x.FLIGHTSFlightId,
                        principalTable: "FLIGHTSFlights",
                        principalColumn: "FLIGHTSFlightId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SHIPSRepairMaterial",
                columns: table => new
                {
                    SHIPSRepairMaterialId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Amount = table.Column<int>(type: "INTEGER", nullable: false),
                    MaterialId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    MaterialName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    MaterialTicker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    SHIPSShipId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SHIPSRepairMaterial", x => x.SHIPSRepairMaterialId);
                    table.ForeignKey(
                        name: "FK_SHIPSRepairMaterial_SHIPSShips_SHIPSShipId",
                        column: x => x.SHIPSShipId,
                        principalTable: "SHIPSShips",
                        principalColumn: "SHIPSShipId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SITESBuildings",
                columns: table => new
                {
                    SITESBuildingId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    BuildingCreated = table.Column<long>(type: "INTEGER", nullable: false),
                    BuildingId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    BuildingLastRepair = table.Column<long>(type: "INTEGER", nullable: true),
                    BuildingName = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    BuildingTicker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    Condition = table.Column<double>(type: "REAL", nullable: false),
                    SITESSiteId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SITESBuildings", x => x.SITESBuildingId);
                    table.ForeignKey(
                        name: "FK_SITESBuildings_SITESSites_SITESSiteId",
                        column: x => x.SITESSiteId,
                        principalTable: "SITESSites",
                        principalColumn: "SITESSiteId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SITESReclaimableMaterials",
                columns: table => new
                {
                    SITESReclaimableMaterialId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    MaterialAmount = table.Column<int>(type: "INTEGER", nullable: false),
                    MaterialId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    MaterialName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    MaterialTicker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    SITESBuildingId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SITESReclaimableMaterials", x => x.SITESReclaimableMaterialId);
                    table.ForeignKey(
                        name: "FK_SITESReclaimableMaterials_SITESBuildings_SITESBuildingId",
                        column: x => x.SITESBuildingId,
                        principalTable: "SITESBuildings",
                        principalColumn: "SITESBuildingId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SITESRepairMaterials",
                columns: table => new
                {
                    SITESRepairMaterialId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    MaterialAmount = table.Column<int>(type: "INTEGER", nullable: false),
                    MaterialId = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    MaterialName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    MaterialTicker = table.Column<string>(type: "TEXT", maxLength: 8, nullable: true),
                    SITESBuildingId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SITESRepairMaterials", x => x.SITESRepairMaterialId);
                    table.ForeignKey(
                        name: "FK_SITESRepairMaterials_SITESBuildings_SITESBuildingId",
                        column: x => x.SITESBuildingId,
                        principalTable: "SITESBuildings",
                        principalColumn: "SITESBuildingId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WorkforceDescriptions_WorkforceModelId",
                table: "WorkforceDescriptions",
                column: "WorkforceModelId");

            migrationBuilder.CreateIndex(
                name: "IX_UserSettingsBurnRates_UserSettingsModelId",
                table: "UserSettingsBurnRates",
                column: "UserSettingsModelId");

            migrationBuilder.CreateIndex(
                name: "IX_SystemConnections_SystemStarsModelId",
                table: "SystemConnections",
                column: "SystemStarsModelId");

            migrationBuilder.CreateIndex(
                name: "IX_SubSectors_WorldSectorsModelId",
                table: "SubSectors",
                column: "WorldSectorsModelId");

            migrationBuilder.CreateIndex(
                name: "IX_StorageItems_StorageModelId",
                table: "StorageItems",
                column: "StorageModelId");

            migrationBuilder.CreateIndex(
                name: "IX_ShippingAds_LocalMarketModelId",
                table: "ShippingAds",
                column: "LocalMarketModelId");

            migrationBuilder.CreateIndex(
                name: "IX_SellingAds_LocalMarketModelId",
                table: "SellingAds",
                column: "LocalMarketModelId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductionLines_PRODLinesModelId",
                table: "ProductionLines",
                column: "PRODLinesModelId");

            migrationBuilder.CreateIndex(
                name: "IX_JumpCacheRoutes_JumpCacheModelId",
                table: "JumpCacheRoutes",
                column: "JumpCacheModelId");

            migrationBuilder.CreateIndex(
                name: "IX_InfrastructureProjectUpkeeps_InfrastructureInfoId",
                table: "InfrastructureProjectUpkeeps",
                column: "InfrastructureInfoId");

            migrationBuilder.CreateIndex(
                name: "IX_InfrastructureProjectUpgradeCosts_InfrastructureInfoId",
                table: "InfrastructureProjectUpgradeCosts",
                column: "InfrastructureInfoId");

            migrationBuilder.CreateIndex(
                name: "IX_InfrastructureProjectContributions_InfrastructureInfoId",
                table: "InfrastructureProjectContributions",
                column: "InfrastructureInfoId");

            migrationBuilder.CreateIndex(
                name: "IX_CXOSTradeOrders_CXOSTradeOrdersModelId",
                table: "CXOSTradeOrders",
                column: "CXOSTradeOrdersModelId");

            migrationBuilder.CreateIndex(
                name: "IX_ContractConditions_ContractModelId",
                table: "ContractConditions",
                column: "ContractModelId");

            migrationBuilder.CreateIndex(
                name: "IX_BuyingAds_LocalMarketModelId",
                table: "BuyingAds",
                column: "LocalMarketModelId");

            migrationBuilder.CreateIndex(
                name: "IX_FLIGHTSOriginLines_FLIGHTSFlightSegmentId",
                table: "FLIGHTSOriginLines",
                column: "FLIGHTSFlightSegmentId");

            migrationBuilder.CreateIndex(
                name: "IX_InfrastructureModelReports_InfrastructureModelId",
                table: "InfrastructureModelReports",
                column: "InfrastructureModelId");

            migrationBuilder.CreateIndex(
                name: "IX_FLIGHTSDestinationLines_FLIGHTSFlightSegmentId",
                table: "FLIGHTSDestinationLines",
                column: "FLIGHTSFlightSegmentId");

            migrationBuilder.CreateIndex(
                name: "IX_BUIBuildingCosts_BUIModelId",
                table: "BUIBuildingCosts",
                column: "BUIModelId");

            migrationBuilder.CreateIndex(
                name: "IX_BuildingDegradationModels_BuildingTicker_DaysSinceLastRepair_HasBeenRepaired",
                table: "BuildingDegradationModels",
                columns: new[] { "BuildingTicker", "DaysSinceLastRepair", "HasBeenRepaired" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BuildingDegradationReclaimableMaterialModels_BuildingDegradationModelId_MaterialTicker",
                table: "BuildingDegradationReclaimableMaterialModels",
                columns: new[] { "BuildingDegradationModelId", "MaterialTicker" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BuildingDegradationRepairMaterialModels_BuildingDegradationModelId_MaterialTicker",
                table: "BuildingDegradationRepairMaterialModels",
                columns: new[] { "BuildingDegradationModelId", "MaterialTicker" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BUIRecipeInputs_BUIRecipeId",
                table: "BUIRecipeInputs",
                column: "BUIRecipeId");

            migrationBuilder.CreateIndex(
                name: "IX_BUIRecipeOutputs_BUIRecipeId",
                table: "BUIRecipeOutputs",
                column: "BUIRecipeId");

            migrationBuilder.CreateIndex(
                name: "IX_BUIRecipes_BUIModelId",
                table: "BUIRecipes",
                column: "BUIModelId");

            migrationBuilder.CreateIndex(
                name: "IX_CompanyDataCurrencyBalances_CompanyDataModelId",
                table: "CompanyDataCurrencyBalances",
                column: "CompanyDataModelId");

            migrationBuilder.CreateIndex(
                name: "IX_CompanyDataModels_CompanyId",
                table: "CompanyDataModels",
                column: "CompanyId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CompanyDataModels_UserName",
                table: "CompanyDataModels",
                column: "UserName");

            migrationBuilder.CreateIndex(
                name: "IX_FLIGHTSFlights_FLIGHTSModelId",
                table: "FLIGHTSFlights",
                column: "FLIGHTSModelId");

            migrationBuilder.CreateIndex(
                name: "IX_FLIGHTSFlightSegments_FLIGHTSFlightId",
                table: "FLIGHTSFlightSegments",
                column: "FLIGHTSFlightId");

            migrationBuilder.CreateIndex(
                name: "IX_InfrastructureInfos_InfrastructureModelId",
                table: "InfrastructureInfos",
                column: "InfrastructureModelId");

            migrationBuilder.CreateIndex(
                name: "IX_SHIPSRepairMaterial_SHIPSShipId",
                table: "SHIPSRepairMaterial",
                column: "SHIPSShipId");

            migrationBuilder.CreateIndex(
                name: "IX_SHIPSShips_SHIPSModelId",
                table: "SHIPSShips",
                column: "SHIPSModelId");

            migrationBuilder.CreateIndex(
                name: "IX_SITESBuildings_SITESSiteId",
                table: "SITESBuildings",
                column: "SITESSiteId");

            migrationBuilder.CreateIndex(
                name: "IX_SITESReclaimableMaterials_SITESBuildingId",
                table: "SITESReclaimableMaterials",
                column: "SITESBuildingId");

            migrationBuilder.CreateIndex(
                name: "IX_SITESRepairMaterials_SITESBuildingId",
                table: "SITESRepairMaterials",
                column: "SITESBuildingId");

            migrationBuilder.CreateIndex(
                name: "IX_SITESSites_SITESModelId",
                table: "SITESSites",
                column: "SITESModelId");

            migrationBuilder.AddForeignKey(
                name: "FK_BuyingAds_LocalMarketModels_LocalMarketModelId",
                table: "BuyingAds",
                column: "LocalMarketModelId",
                principalTable: "LocalMarketModels",
                principalColumn: "LocalMarketModelId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ContractConditions_ContractModels_ContractModelId",
                table: "ContractConditions",
                column: "ContractModelId",
                principalTable: "ContractModels",
                principalColumn: "ContractModelId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ContractDependencies_ContractConditions_ContractConditionId",
                table: "ContractDependencies",
                column: "ContractConditionId",
                principalTable: "ContractConditions",
                principalColumn: "ContractConditionId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CXOSTradeOrders_CXOSTradeOrderModels_CXOSTradeOrdersModelId",
                table: "CXOSTradeOrders",
                column: "CXOSTradeOrdersModelId",
                principalTable: "CXOSTradeOrderModels",
                principalColumn: "CXOSTradeOrdersModelId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CXOSTrades_CXOSTradeOrders_CXOSTradeOrderId",
                table: "CXOSTrades",
                column: "CXOSTradeOrderId",
                principalTable: "CXOSTradeOrders",
                principalColumn: "CXOSTradeOrderId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CXPCDataEntries_CXPCData_CXPCDataId",
                table: "CXPCDataEntries",
                column: "CXPCDataId",
                principalTable: "CXPCData",
                principalColumn: "CXPCDataId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FLIGHTSDestinationLines_FLIGHTSFlightSegments_FLIGHTSFlightSegmentId",
                table: "FLIGHTSDestinationLines",
                column: "FLIGHTSFlightSegmentId",
                principalTable: "FLIGHTSFlightSegments",
                principalColumn: "FLIGHTSFlightSegmentId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FLIGHTSOriginLines_FLIGHTSFlightSegments_FLIGHTSFlightSegmentId",
                table: "FLIGHTSOriginLines",
                column: "FLIGHTSFlightSegmentId",
                principalTable: "FLIGHTSFlightSegments",
                principalColumn: "FLIGHTSFlightSegmentId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_InfrastructureModelReports_InfrastructureModels_InfrastructureModelId",
                table: "InfrastructureModelReports",
                column: "InfrastructureModelId",
                principalTable: "InfrastructureModels",
                principalColumn: "InfrastructureModelId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_InfrastructureProjectContributions_InfrastructureInfos_InfrastructureInfoId",
                table: "InfrastructureProjectContributions",
                column: "InfrastructureInfoId",
                principalTable: "InfrastructureInfos",
                principalColumn: "InfrastructureInfoId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_InfrastructureProjectUpgradeCosts_InfrastructureInfos_InfrastructureInfoId",
                table: "InfrastructureProjectUpgradeCosts",
                column: "InfrastructureInfoId",
                principalTable: "InfrastructureInfos",
                principalColumn: "InfrastructureInfoId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_InfrastructureProjectUpkeeps_InfrastructureInfos_InfrastructureInfoId",
                table: "InfrastructureProjectUpkeeps",
                column: "InfrastructureInfoId",
                principalTable: "InfrastructureInfos",
                principalColumn: "InfrastructureInfoId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_JumpCacheRoutes_JumpCacheModels_JumpCacheModelId",
                table: "JumpCacheRoutes",
                column: "JumpCacheModelId",
                principalTable: "JumpCacheModels",
                principalColumn: "JumpCacheModelId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductionLineInputs_ProductionLineOrders_ProductionLineOrderId",
                table: "ProductionLineInputs",
                column: "ProductionLineOrderId",
                principalTable: "ProductionLineOrders",
                principalColumn: "ProductionLineOrderId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductionLineOrders_ProductionLines_ProductionLineId",
                table: "ProductionLineOrders",
                column: "ProductionLineId",
                principalTable: "ProductionLines",
                principalColumn: "ProductionLineId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductionLineOutputs_ProductionLineOrders_ProductionLineOrderId",
                table: "ProductionLineOutputs",
                column: "ProductionLineOrderId",
                principalTable: "ProductionLineOrders",
                principalColumn: "ProductionLineOrderId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductionLines_PRODLinesModels_PRODLinesModelId",
                table: "ProductionLines",
                column: "PRODLinesModelId",
                principalTable: "PRODLinesModels",
                principalColumn: "PRODLinesModelId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SellingAds_LocalMarketModels_LocalMarketModelId",
                table: "SellingAds",
                column: "LocalMarketModelId",
                principalTable: "LocalMarketModels",
                principalColumn: "LocalMarketModelId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ShippingAds_LocalMarketModels_LocalMarketModelId",
                table: "ShippingAds",
                column: "LocalMarketModelId",
                principalTable: "LocalMarketModels",
                principalColumn: "LocalMarketModelId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_StorageItems_StorageModels_StorageModelId",
                table: "StorageItems",
                column: "StorageModelId",
                principalTable: "StorageModels",
                principalColumn: "StorageModelId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SubSectors_WorldSectorsModels_WorldSectorsModelId",
                table: "SubSectors",
                column: "WorldSectorsModelId",
                principalTable: "WorldSectorsModels",
                principalColumn: "WorldSectorsModelId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SubSectorVertices_SubSectors_SubSectorId",
                table: "SubSectorVertices",
                column: "SubSectorId",
                principalTable: "SubSectors",
                principalColumn: "SubSectorId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SystemConnections_SystemStarsModels_SystemStarsModelId",
                table: "SystemConnections",
                column: "SystemStarsModelId",
                principalTable: "SystemStarsModels",
                principalColumn: "SystemStarsModelId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserSettingsBurnRateExclusions_UserSettingsBurnRates_UserSettingsBurnRateId",
                table: "UserSettingsBurnRateExclusions",
                column: "UserSettingsBurnRateId",
                principalTable: "UserSettingsBurnRates",
                principalColumn: "UserSettingsBurnRateId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserSettingsBurnRates_UserSettingsModels_UserSettingsModelId",
                table: "UserSettingsBurnRates",
                column: "UserSettingsModelId",
                principalTable: "UserSettingsModels",
                principalColumn: "UserSettingsModelId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_WorkforceDescriptions_WorkforceModels_WorkforceModelId",
                table: "WorkforceDescriptions",
                column: "WorkforceModelId",
                principalTable: "WorkforceModels",
                principalColumn: "WorkforceModelId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_WorkforceNeeds_WorkforceDescriptions_WorkforceDescriptionId",
                table: "WorkforceNeeds",
                column: "WorkforceDescriptionId",
                principalTable: "WorkforceDescriptions",
                principalColumn: "WorkforceDescriptionId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
