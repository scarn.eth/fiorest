﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FIORest.Migrations.SqliteMigrations
{
    public partial class AddBuildingDegredation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BuildingDegradationModels",
                columns: table => new
                {
                    BuildingDegradationModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    BuildingTicker = table.Column<string>(type: "TEXT", maxLength: 10, nullable: true),
                    HasBeenRepaired = table.Column<bool>(type: "INTEGER", nullable: false),
                    Condition = table.Column<double>(type: "REAL", nullable: false),
                    DaysSinceLastRepair = table.Column<double>(type: "REAL", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuildingDegradationModels", x => x.BuildingDegradationModelId);
                });

            migrationBuilder.CreateTable(
                name: "BuildingDegradationReclaimableMaterialModels",
                columns: table => new
                {
                    BuildingDegradationReclaimableMaterialModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    MaterialTicker = table.Column<string>(type: "TEXT", nullable: true),
                    MaterialCount = table.Column<int>(type: "INTEGER", nullable: false),
                    BuildingDegradationModelId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuildingDegradationReclaimableMaterialModels", x => x.BuildingDegradationReclaimableMaterialModelId);
                    table.ForeignKey(
                        name: "FK_BuildingDegradationReclaimableMaterialModels_BuildingDegradationModels_BuildingDegradationModelId",
                        column: x => x.BuildingDegradationModelId,
                        principalTable: "BuildingDegradationModels",
                        principalColumn: "BuildingDegradationModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BuildingDegradationRepairMaterialModels",
                columns: table => new
                {
                    BuildingDegradationRepairMaterialModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    MaterialTicker = table.Column<string>(type: "TEXT", nullable: true),
                    MaterialCount = table.Column<int>(type: "INTEGER", nullable: false),
                    BuildingDegradationModelId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuildingDegradationRepairMaterialModels", x => x.BuildingDegradationRepairMaterialModelId);
                    table.ForeignKey(
                        name: "FK_BuildingDegradationRepairMaterialModels_BuildingDegradationModels_BuildingDegradationModelId",
                        column: x => x.BuildingDegradationModelId,
                        principalTable: "BuildingDegradationModels",
                        principalColumn: "BuildingDegradationModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BuildingDegradationModels_BuildingTicker_DaysSinceLastRepair_HasBeenRepaired",
                table: "BuildingDegradationModels",
                columns: new[] { "BuildingTicker", "DaysSinceLastRepair", "HasBeenRepaired" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BuildingDegradationReclaimableMaterialModels_BuildingDegradationModelId_MaterialTicker",
                table: "BuildingDegradationReclaimableMaterialModels",
                columns: new[] { "BuildingDegradationModelId", "MaterialTicker" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BuildingDegradationRepairMaterialModels_BuildingDegradationModelId_MaterialTicker",
                table: "BuildingDegradationRepairMaterialModels",
                columns: new[] { "BuildingDegradationModelId", "MaterialTicker" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BuildingDegradationReclaimableMaterialModels");

            migrationBuilder.DropTable(
                name: "BuildingDegradationRepairMaterialModels");

            migrationBuilder.DropTable(
                name: "BuildingDegradationModels");
        }
    }
}
