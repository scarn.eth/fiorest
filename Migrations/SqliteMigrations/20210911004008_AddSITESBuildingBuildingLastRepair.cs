﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FIORest.Migrations.SqliteMigrations
{
    public partial class AddSITESBuildingBuildingLastRepair : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "BuildingLastRepair",
                table: "SITESBuildings",
                type: "INTEGER",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BuildingLastRepair",
                table: "SITESBuildings");
        }
    }
}
