﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FIORest.Migrations.PostgresMigrations
{
    public partial class AddShipLocationAddressLines : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ShipLocationAddressLines",
                columns: table => new
                {
                    ShipLocationAddressLineId = table.Column<string>(type: "character varying(65)", maxLength: 65, nullable: false),
                    LineId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    LineType = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: true),
                    NaturalId = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    Name = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    ShipId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShipLocationAddressLines", x => x.ShipLocationAddressLineId);
                    table.ForeignKey(
                        name: "FK_ShipLocationAddressLines_Ships_ShipId",
                        column: x => x.ShipId,
                        principalTable: "Ships",
                        principalColumn: "ShipId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ShipLocationAddressLines_ShipId",
                table: "ShipLocationAddressLines",
                column: "ShipId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ShipLocationAddressLines");
        }
    }
}
