﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace FIORest.Migrations.PostgresMigrations
{
    public partial class AddBuildingDegredation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BuildingDegradationModels",
                columns: table => new
                {
                    BuildingDegradationModelId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    BuildingTicker = table.Column<string>(type: "character varying(10)", maxLength: 10, nullable: true),
                    HasBeenRepaired = table.Column<bool>(type: "boolean", nullable: false),
                    Condition = table.Column<double>(type: "double precision", nullable: false),
                    DaysSinceLastRepair = table.Column<double>(type: "double precision", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuildingDegradationModels", x => x.BuildingDegradationModelId);
                });

            migrationBuilder.CreateTable(
                name: "BuildingDegradationReclaimableMaterialModels",
                columns: table => new
                {
                    BuildingDegradationReclaimableMaterialModelId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    MaterialTicker = table.Column<string>(type: "text", nullable: true),
                    MaterialCount = table.Column<int>(type: "integer", nullable: false),
                    BuildingDegradationModelId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuildingDegradationReclaimableMaterialModels", x => x.BuildingDegradationReclaimableMaterialModelId);
                    table.ForeignKey(
                        name: "FK_BuildingDegradationReclaimableMaterialModels_BuildingDegrad~",
                        column: x => x.BuildingDegradationModelId,
                        principalTable: "BuildingDegradationModels",
                        principalColumn: "BuildingDegradationModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BuildingDegradationRepairMaterialModels",
                columns: table => new
                {
                    BuildingDegradationRepairMaterialModelId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    MaterialTicker = table.Column<string>(type: "text", nullable: true),
                    MaterialCount = table.Column<int>(type: "integer", nullable: false),
                    BuildingDegradationModelId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuildingDegradationRepairMaterialModels", x => x.BuildingDegradationRepairMaterialModelId);
                    table.ForeignKey(
                        name: "FK_BuildingDegradationRepairMaterialModels_BuildingDegradation~",
                        column: x => x.BuildingDegradationModelId,
                        principalTable: "BuildingDegradationModels",
                        principalColumn: "BuildingDegradationModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BuildingDegradationModels_BuildingTicker_DaysSinceLastRepai~",
                table: "BuildingDegradationModels",
                columns: new[] { "BuildingTicker", "DaysSinceLastRepair", "HasBeenRepaired" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BuildingDegradationReclaimableMaterialModels_BuildingDegrad~",
                table: "BuildingDegradationReclaimableMaterialModels",
                columns: new[] { "BuildingDegradationModelId", "MaterialTicker" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BuildingDegradationRepairMaterialModels_BuildingDegradation~",
                table: "BuildingDegradationRepairMaterialModels",
                columns: new[] { "BuildingDegradationModelId", "MaterialTicker" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BuildingDegradationReclaimableMaterialModels");

            migrationBuilder.DropTable(
                name: "BuildingDegradationRepairMaterialModels");

            migrationBuilder.DropTable(
                name: "BuildingDegradationModels");
        }
    }
}
