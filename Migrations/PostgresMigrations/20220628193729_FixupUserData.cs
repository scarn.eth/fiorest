﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FIORest.Migrations.PostgresMigrations
{
    public partial class FixupUserData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "SystemNamingRights",
                table: "UserData",
                newName: "HeadquartersUsedBasePermits");

            migrationBuilder.RenameColumn(
                name: "PlanetNamingRights",
                table: "UserData",
                newName: "HeadquartersLevel");

            migrationBuilder.RenameColumn(
                name: "IsPayingUser",
                table: "UserData",
                newName: "RelocationLocked");

            migrationBuilder.RenameColumn(
                name: "IsModeratorChat",
                table: "UserData",
                newName: "Moderator");

            migrationBuilder.AddColumn<string>(
                name: "ActivityRating",
                table: "UserData",
                type: "character varying(8)",
                maxLength: 8,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "AdditionalBasePermits",
                table: "UserData",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "AdditionalProductionQueueSlots",
                table: "UserData",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "CompanyCode",
                table: "UserData",
                type: "character varying(4)",
                maxLength: 4,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CompanyId",
                table: "UserData",
                type: "character varying(32)",
                maxLength: 32,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CompanyName",
                table: "UserData",
                type: "character varying(64)",
                maxLength: 64,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CorporationCode",
                table: "UserData",
                type: "character varying(8)",
                maxLength: 8,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CorporationId",
                table: "UserData",
                type: "character varying(32)",
                maxLength: 32,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CorporationName",
                table: "UserData",
                type: "character varying(64)",
                maxLength: 64,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CountryCode",
                table: "UserData",
                type: "character varying(4)",
                maxLength: 4,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CountryId",
                table: "UserData",
                type: "character varying(32)",
                maxLength: 32,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CountryName",
                table: "UserData",
                type: "character varying(64)",
                maxLength: 64,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "HeadquartersBasePermits",
                table: "UserData",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "HeadquartersNaturalId",
                table: "UserData",
                type: "character varying(8)",
                maxLength: 8,
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "NextRelocationTimeEpochMs",
                table: "UserData",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<string>(
                name: "OverallRating",
                table: "UserData",
                type: "character varying(8)",
                maxLength: 8,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReliabilityRating",
                table: "UserData",
                type: "character varying(8)",
                maxLength: 8,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StabilityRating",
                table: "UserData",
                type: "character varying(8)",
                maxLength: 8,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SubscriptionLevel",
                table: "UserData",
                type: "character varying(16)",
                maxLength: 16,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserId",
                table: "UserData",
                type: "character varying(32)",
                maxLength: 32,
                nullable: true);

            migrationBuilder.CreateTable(
                name: "UserDataBalances",
                columns: table => new
                {
                    UserDataBalanceId = table.Column<string>(type: "character varying(40)", maxLength: 40, nullable: false),
                    Currency = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    Amount = table.Column<double>(type: "double precision", nullable: false),
                    UserDataId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserDataBalances", x => x.UserDataBalanceId);
                    table.ForeignKey(
                        name: "FK_UserDataBalances_UserData_UserDataId",
                        column: x => x.UserDataId,
                        principalTable: "UserData",
                        principalColumn: "UserDataId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserDataPlanets",
                columns: table => new
                {
                    UserDataPlanetId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    PlanetNaturalId = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    PlanetName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    UserDataId = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserDataPlanets", x => x.UserDataPlanetId);
                    table.ForeignKey(
                        name: "FK_UserDataPlanets_UserData_UserDataId",
                        column: x => x.UserDataId,
                        principalTable: "UserData",
                        principalColumn: "UserDataId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserData_CompanyId",
                table: "UserData",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_UserDataBalances_UserDataId",
                table: "UserDataBalances",
                column: "UserDataId");

            migrationBuilder.CreateIndex(
                name: "IX_UserDataPlanets_UserDataId",
                table: "UserDataPlanets",
                column: "UserDataId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserDataBalances");

            migrationBuilder.DropTable(
                name: "UserDataPlanets");

            migrationBuilder.DropIndex(
                name: "IX_UserData_CompanyId",
                table: "UserData");

            migrationBuilder.DropColumn(
                name: "ActivityRating",
                table: "UserData");

            migrationBuilder.DropColumn(
                name: "AdditionalBasePermits",
                table: "UserData");

            migrationBuilder.DropColumn(
                name: "AdditionalProductionQueueSlots",
                table: "UserData");

            migrationBuilder.DropColumn(
                name: "CompanyCode",
                table: "UserData");

            migrationBuilder.DropColumn(
                name: "CompanyId",
                table: "UserData");

            migrationBuilder.DropColumn(
                name: "CompanyName",
                table: "UserData");

            migrationBuilder.DropColumn(
                name: "CorporationCode",
                table: "UserData");

            migrationBuilder.DropColumn(
                name: "CorporationId",
                table: "UserData");

            migrationBuilder.DropColumn(
                name: "CorporationName",
                table: "UserData");

            migrationBuilder.DropColumn(
                name: "CountryCode",
                table: "UserData");

            migrationBuilder.DropColumn(
                name: "CountryId",
                table: "UserData");

            migrationBuilder.DropColumn(
                name: "CountryName",
                table: "UserData");

            migrationBuilder.DropColumn(
                name: "HeadquartersBasePermits",
                table: "UserData");

            migrationBuilder.DropColumn(
                name: "HeadquartersNaturalId",
                table: "UserData");

            migrationBuilder.DropColumn(
                name: "NextRelocationTimeEpochMs",
                table: "UserData");

            migrationBuilder.DropColumn(
                name: "OverallRating",
                table: "UserData");

            migrationBuilder.DropColumn(
                name: "ReliabilityRating",
                table: "UserData");

            migrationBuilder.DropColumn(
                name: "StabilityRating",
                table: "UserData");

            migrationBuilder.DropColumn(
                name: "SubscriptionLevel",
                table: "UserData");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "UserData");

            migrationBuilder.RenameColumn(
                name: "RelocationLocked",
                table: "UserData",
                newName: "IsPayingUser");

            migrationBuilder.RenameColumn(
                name: "Moderator",
                table: "UserData",
                newName: "IsModeratorChat");

            migrationBuilder.RenameColumn(
                name: "HeadquartersUsedBasePermits",
                table: "UserData",
                newName: "SystemNamingRights");

            migrationBuilder.RenameColumn(
                name: "HeadquartersLevel",
                table: "UserData",
                newName: "PlanetNamingRights");
        }
    }
}
