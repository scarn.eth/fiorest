﻿#if WITH_MODULES
using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.EntityFrameworkCore;
using Nancy;
using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;


namespace FIORest.Modules.FIOWeb.Burn
{
    public class BurnModule : NancyModule
    {
        public BurnModule() : base("/fioweb/burn")
        {
            Get("/user/{username}", parameters =>
            {
                return GetBurnForUsername(parameters.username);
            });

            Get("/users/{usernames}", parameters =>
            {
                return GetBurnForUsernames(parameters.usernames);
            });

            Get("/group/{groupid}", parameters =>
            {
                return GetBurnForGroupId(parameters.groupid);
            });
        }

        public static List<BurnPayload> GetBurnForListOfUserNames(string RequesterUserName, List<string> RequestedUserNames, string ProxyRequesterUserName = null)
        {
            var burnPlanets = new List<BurnPayload>();

            if (RequesterUserName == "FIDO" && ProxyRequesterUserName == null)
            {
                // Don't allow general fido requests
                return null;
            }

            // Uppercase all UserNames
            RequestedUserNames = RequestedUserNames.Select(name => name.ToUpper()).ToList();

            using (var DB = PRUNDataContext.GetNewContext())
            using (PerfTracer overall = new PerfTracer("Overall"))
            {
                Dictionary<string, PermissionAllowance> RequesterPermissions = Caches.PermissionAllowancesCache.GetOrCache(RequesterUserName, DB);
                Dictionary<string, PermissionAllowance> ProxyRequesterPermissions = ProxyRequesterUserName != null ? Caches.PermissionAllowancesCache.GetOrCache(ProxyRequesterUserName, DB) : null;

                List<string> UserNames = new List<string>();
                foreach (var UserName in RequestedUserNames)
                {
                    using (PerfTracer pf = new PerfTracer("CanSeeData"))
                    {
                        bool RequesterHasUserNameKey = RequesterPermissions.ContainsKey(UserName);

                        bool HasStoragePermissions = RequesterHasUserNameKey && RequesterPermissions[UserName].StorageData;
                        bool HasBuildingPermissions = RequesterHasUserNameKey && RequesterPermissions[UserName].BuildingData;
                        bool HasProductionPermissions = RequesterHasUserNameKey && RequesterPermissions[UserName].ProductionData;
                        bool HasWorkforcePermissions = RequesterHasUserNameKey && RequesterPermissions[UserName].WorkforceData;

                        if (ProxyRequesterUserName != null)
                        {
                            bool ProxyRequesterHasUserNameKey = ProxyRequesterPermissions.ContainsKey(UserName);

                            HasStoragePermissions &= ProxyRequesterHasUserNameKey && ProxyRequesterPermissions[UserName].StorageData;
                            HasBuildingPermissions &= ProxyRequesterHasUserNameKey && ProxyRequesterPermissions[UserName].BuildingData;
                            HasProductionPermissions &= ProxyRequesterHasUserNameKey && ProxyRequesterPermissions[UserName].ProductionData;
                            HasWorkforcePermissions &= ProxyRequesterHasUserNameKey && ProxyRequesterPermissions[UserName].WorkforceData;
                        }

                        if (!HasStoragePermissions || !HasBuildingPermissions || !HasProductionPermissions || !HasWorkforcePermissions)
                        {
                            var errorPayload = new BurnPayload();
                            errorPayload.RequesterUserName = (ProxyRequesterUserName != null) ? ProxyRequesterUserName : RequesterUserName;
                            errorPayload.UserName = UserName;
                            errorPayload.Error = "Insufficient Permission. Requires Storage, Building, Production, and Workforce";
                            burnPlanets.Add(errorPayload);
                            continue;
                        }
                        UserNames.Add(UserName);
                    }
                }

                List<Storage> storages = null;
                List<Site> sites = null;
                List<Workforce> workforces = null;
                List<ProductionLine> productionLines = null;

                using (PerfTracer pf2 = new PerfTracer($"Data retrieval"))
                {
                    storages = DB.Storages
                        .AsNoTracking()
                        .Include(sm => sm.StorageItems)
                        .Where(s => UserNames.Contains(s.UserNameSubmitted))
                        .AsSplitQuery()
                        .ToList();
                    sites = DB.Sites
                        .AsNoTracking()
                        .Include(site => site.Buildings)
                            .ThenInclude(bui => bui.ReclaimableMaterials)
                        .Include(site => site.Buildings)
                            .ThenInclude(b => b.RepairMaterials)
                        .Where(s => UserNames.Contains(s.UserNameSubmitted))
                        .AsSplitQuery()
                        .ToList();
                    workforces = DB.Workforces
                        .AsNoTracking()
                        .Include(wm => wm.Workforces)
                            .ThenInclude(w => w.WorkforceNeeds)
                        .Where(s => UserNames.Contains(s.UserNameSubmitted))
                        .AsSplitQuery()
                        .ToList();
                    productionLines = DB.ProductionLines
                        .AsNoTracking()
                        .Include(plm => plm.Orders)
                            .ThenInclude(o => o.Inputs)
                        .Include(plm => plm.Orders)
                            .ThenInclude(o => o.Outputs)
                        .Where(p => UserNames.Contains(p.UserNameSubmitted))
                        .AsSplitQuery()
                        .ToList();
                }

                foreach(var UserName in UserNames)
                {
                    var mysites = sites.Where(item => item.UserNameSubmitted == UserName).ToList();
                    var mystorages = storages.Where(item => item.UserNameSubmitted == UserName).ToList();
                    var myworkforces = workforces.Where(item => item.UserNameSubmitted == UserName).ToList();
                    var myproductions = productionLines.Where(item => item.UserNameSubmitted == UserName).ToList();

                    bool bSitesValid = mysites.Count > 0;
                    bool bStorageValid = mystorages.Count > 0;
                    bool bWorkforcesValid = myworkforces.Count > 0;
                    bool bProductionsValid = myproductions.Count > 0;

                    if (!bSitesValid || !bStorageValid || !bWorkforcesValid || !bProductionsValid)
                    {
                        var errorPayload = new BurnPayload();
                        errorPayload.RequesterUserName = (ProxyRequesterUserName != null) ? ProxyRequesterUserName : RequesterUserName;
                        errorPayload.UserName = UserName;

                        errorPayload.Error = "Failed to find any of the following data: ";
                        if (!bSitesValid) errorPayload.Error += "Sites ";
                        if (!bStorageValid) errorPayload.Error += "Storage ";
                        if (!bWorkforcesValid) errorPayload.Error += "Workforce ";
                        if (!bProductionsValid) errorPayload.Error += "Production ";
                        errorPayload.Error = errorPayload.Error.Trim();

                        burnPlanets.Add(errorPayload);
                    }
                    else
                    {
                        //using(PerfTracer p4 = new PerfTracer($"ForEachSite"))
                        {
                            // Grab each planet
                            foreach (var site in mysites)
                            {
                                var burnPlanet = new BurnPayload();
                                burnPlanet.RequesterUserName = (ProxyRequesterUserName != null) ? ProxyRequesterUserName : RequesterUserName;
                                burnPlanet.UserName = UserName;
                                burnPlanet.PlanetId = site.PlanetId;
                                burnPlanet.PlanetName = site.PlanetName;
                                burnPlanet.PlanetNaturalId = site.PlanetIdentifier;

                                using (PerfTracer p5 = new PerfTracer($"StorageItems"))
                                {
                                    // Add all the storage items
                                    var planetStorageItems = mystorages.FirstOrDefault(s => s.Type == "STORE" && s.AddressableId.ToUpper() == site.SiteId.ToUpper());
                                    if (planetStorageItems != null)
                                    {
                                        if (planetStorageItems.Timestamp > burnPlanet.LastUpdate)
                                        {
                                            burnPlanet.LastUpdate = planetStorageItems.Timestamp;
                                            burnPlanet.LastUpdateCause = "STORAGE";
                                        }

                                        foreach (var planetStorageItem in planetStorageItems.StorageItems)
                                        {
                                            var storageItem = new BurnInventoryItem();
                                            storageItem.MaterialId = planetStorageItem.MaterialId;
                                            storageItem.MaterialTicker = planetStorageItem.MaterialTicker;
                                            storageItem.MaterialAmount = planetStorageItem.MaterialAmount;
                                            burnPlanet.Inventory.Add(storageItem);
                                        }
                                    }
                                }

                                using (var pf6 = new PerfTracer($"WorkforceConsumption"))
                                {
                                    // Add the workforce consumption
                                    var planetWorkforce = myworkforces.FirstOrDefault(w => w.SiteId.ToUpper() == site.SiteId.ToUpper());
                                    if (planetWorkforce != null)
                                    {
                                        if (planetWorkforce.Timestamp > burnPlanet.LastUpdate)
                                        {
                                            burnPlanet.LastUpdate = planetWorkforce.Timestamp;
                                            burnPlanet.LastUpdateCause = "WORKFORCE";
                                        }

                                        burnPlanet.PlanetConsumptionTime = planetWorkforce.LastWorkforceUpdateTime;

                                        var workforceNeeds = planetWorkforce.Workforces.SelectMany(w => w.WorkforceNeeds);
                                        foreach (var workforceNeed in workforceNeeds)
                                        {
                                            if (workforceNeed.UnitsPerInterval > 0.0)
                                            {
                                                var consumption = burnPlanet.WorkforceConsumption.FirstOrDefault(wc => wc.MaterialId == workforceNeed.MaterialId);
                                                if (consumption == null)
                                                {
                                                    consumption = new BurnConsumptionProduction();
                                                    burnPlanet.WorkforceConsumption.Add(consumption);
                                                }
                                                consumption.MaterialId = workforceNeed.MaterialId;
                                                consumption.MaterialTicker = workforceNeed.MaterialTicker;
                                                consumption.DailyAmount += workforceNeed.UnitsPerInterval;
                                            }
                                        }
                                    }
                                }

                                using (var pf7 = new PerfTracer($"Production"))
                                {
                                    var planetProdLines = myproductions.Where(pl => pl.SiteId.ToUpper() == site.SiteId.ToUpper()).ToList();
                                    if (planetProdLines.Count > 0)
                                    {
                                        var MillisecondsInADay = (long)TimeSpan.FromDays(1.0).TotalMilliseconds;
                                        foreach (var prodLine in planetProdLines)
                                        {
                                            if (prodLine.Timestamp > burnPlanet.LastUpdate)
                                            {
                                                burnPlanet.LastUpdate = prodLine.Timestamp;
                                                burnPlanet.LastUpdateCause = "PRODUCTION";
                                            }

                                            long? totalRecurringOrderTime = 0;
                                            List<ProductionLineOrder> recurringOrders;
                                            //using(var pf8 = new PerfTracer("ProdLineQueries"))
                                            {
                                                recurringOrders = prodLine.Orders
                                                    .Where(o => o.Recurring && o.StartedEpochMs == null)
                                                    .ToList();

                                                if (recurringOrders.Count == 0)
                                                {
                                                    // Fallback to using just the regular pending queued orders
                                                    recurringOrders = prodLine.Orders
                                                        .Where(o => o.StartedEpochMs == null)
                                                        .ToList();
                                                }

                                                // DurationMs of 0 means = Order is halted.
                                                totalRecurringOrderTime = recurringOrders
                                                    .Where(ro => ro.DurationMs != null && (long)ro.DurationMs > 0)
                                                    .Sum(ro => ro.DurationMs);
                                            }

                                            if (recurringOrders.Count > 0 && totalRecurringOrderTime != null && totalRecurringOrderTime > 0)
                                            {
                                                var prodLineCapacity = prodLine.Capacity;
                                                var prodLineTotalTimeMs = MillisecondsInADay * prodLine.Capacity;

                                                foreach (var order in recurringOrders)
                                                {
                                                    var orderPercentage = (double)order.DurationMs / (double)totalRecurringOrderTime;

                                                    foreach (var input in order.Inputs)
                                                    {
                                                        var orderConsumption = burnPlanet.OrderConsumption.FirstOrDefault(oc => oc.MaterialId == input.MaterialId);
                                                        if (orderConsumption == null)
                                                        {
                                                            orderConsumption = new BurnConsumptionProduction();
                                                            burnPlanet.OrderConsumption.Add(orderConsumption);
                                                        }

                                                        orderConsumption.MaterialId = input.MaterialId;
                                                        orderConsumption.MaterialTicker = input.MaterialTicker;

                                                        var numOrders = (double)orderPercentage * (prodLineTotalTimeMs / (double)order.DurationMs);
                                                        orderConsumption.DailyAmount += input.MaterialAmount * numOrders;
                                                    }

                                                    foreach (var output in order.Outputs)
                                                    {
                                                        var orderProduction = burnPlanet.OrderProduction.FirstOrDefault(op => op.MaterialId == output.MaterialId);
                                                        if (orderProduction == null)
                                                        {
                                                            orderProduction = new BurnConsumptionProduction();
                                                            burnPlanet.OrderProduction.Add(orderProduction);
                                                        }

                                                        orderProduction.MaterialId = output.MaterialId;
                                                        orderProduction.MaterialTicker = output.MaterialTicker;

                                                        var numOrders = (double)orderPercentage * (prodLineTotalTimeMs / (double)order.DurationMs);
                                                        orderProduction.DailyAmount += output.MaterialAmount * numOrders;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                burnPlanets.Add(burnPlanet);
                            }
                        }
                    }
                }
            }

            return burnPlanets;
        }

        public Response GetBurnForGroupId(string GroupIdStr)
        {
            if (int.TryParse(GroupIdStr, out int GroupId))
            {
                string ProxyRequesterUserName = null;

                List<string> listOfUserNames;
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var group = DB.GroupModels
                        .AsNoTracking()
                        .Include(gm => gm.GroupUsers)
                        .Include(gm => gm.GroupAdmins)
                        .FirstOrDefault(gm => gm.GroupModelId == GroupId);
                    if (group != null)
                    {
                        listOfUserNames = group.GroupUsers.Select(gu => gu.GroupUserName).ToList();
                    }
                    else
                    {
                        return HttpStatusCode.NotFound;
                    }

                    if (Request.Headers.Keys.Contains("ProxyRequesterDiscordId"))
                    {
                        var ProxyDiscordId = Request.Headers["ProxyRequesterDiscordId"].FirstOrDefault();
                        ProxyRequesterUserName = DB.AuthenticationModels.Where(am => am.DiscordId.ToUpper() == ProxyDiscordId.ToUpper()).Select(am => am.UserName).FirstOrDefault();
                    }
                }

                var result = GetBurnForListOfUserNames(Request.GetUserName(), listOfUserNames, ProxyRequesterUserName);
                if (result == null)
                {
                    return HttpStatusCode.Unauthorized;
                }
                return JsonConvert.SerializeObject(result);
            }

            Response resp = "Invalid GroupId Specified";
            resp.ContentType = "text/plain";
            resp.StatusCode = HttpStatusCode.BadRequest;
            return resp;
        }

        public Response GetBurnForUsernames(string UserNames)
        {
            var listOfUserNames = UserNames.Split(new string[] { "__" }, StringSplitOptions.RemoveEmptyEntries).ToList();
            var result = GetBurnForListOfUserNames(Request.GetUserName(), listOfUserNames);
            if (result == null)
            {
                return HttpStatusCode.Unauthorized;
            }
            return JsonConvert.SerializeObject(result);
        }

        public Response GetBurnForUsername(string UserName)
        {
            var result = GetBurnForListOfUserNames(Request.GetUserName(), new List<string> { UserName });
            if (result == null)
            {
                return HttpStatusCode.Unauthorized;
            }
            return JsonConvert.SerializeObject(result);
        }
    }
}
#endif // WITH_MODULES