﻿#if WITH_MODULES
using System.Linq;

using Nancy;

using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;
using Microsoft.EntityFrameworkCore;

namespace FIORest.Modules
{
    public class WorkforceModule : NancyModule
    {
        public WorkforceModule() : base("/workforce")
        {
            Post("/", _ =>
            {
                this.EnforceWriteAuth();
                return PostWorkforce();
            });

            Post("/updated", _ =>
            {
                this.EnforceWriteAuth();
                return PostWorkforceUpdated();
            });

            Get("/{username}", parameters =>
            {
                this.EnforceReadAuth();
                return GetWorkforce(parameters.username);
            });

            Get("/planets/{username}", parameters =>
            {
                this.EnforceReadAuth();
                return GetWorkforcePlanets(parameters.username);
            });

            Get("/{username}/{planet}", parameters =>
            {
                this.EnforceReadAuth();
                return GetWorkforce(parameters.username, parameters.planet);
            });
        }

        private Response PostWorkforce()
        {
            using (var req = new FIORequest<JSONRepresentations.WorkforceWorkforces.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var data = req.JsonPayload.payload.message.payload;

                var wk = new Workforce();
                wk.WorkforceId = data.siteId;

                wk.PlanetId = data.address.lines[1].entity.id;
                wk.PlanetNaturalId = data.address.lines[1].entity.naturalId;
                wk.PlanetName = data.address.lines[1].entity.name;

                wk.SiteId = data.siteId;

                foreach (var workforce in data.workforces)
                {
                    WorkforceDescription workforceDesc = new WorkforceDescription();
                    workforceDesc.WorkforceDescriptionId = $"{wk.WorkforceId}-{workforce.level}";
                    workforceDesc.WorkforceTypeName = workforce.level;
                    workforceDesc.Population = workforce.population;
                    workforceDesc.Reserve = workforce.reserve;
                    workforceDesc.Capacity = workforce.capacity;
                    workforceDesc.Required = workforce.required;
                    workforceDesc.Satisfaction = workforce.satisfaction;
                    workforceDesc.WorkforceId = wk.WorkforceId;

                    foreach (var need in workforce.needs)
                    {
                        WorkforceNeed workforceNeed = new WorkforceNeed();
                        workforceNeed.WorkforceNeedId = $"{workforceDesc.WorkforceDescriptionId}-{need.material.ticker}";
                        workforceNeed.Category = need.category;
                        workforceNeed.Essential = need.essential;
                        workforceNeed.MaterialId = need.material.id;
                        workforceNeed.MaterialName = need.material.name;
                        workforceNeed.MaterialTicker = need.material.ticker;

                        workforceNeed.Satisfaction = need.satisfaction;
                        workforceNeed.UnitsPerInterval = need.unitsPerInterval;
                        workforceNeed.UnitsPerOneHundred = need.unitsPer100;
                        workforceNeed.WorkforceDescriptionId = workforceDesc.WorkforceDescriptionId;

                        workforceDesc.WorkforceNeeds.Add(workforceNeed);
                    }

                    wk.Workforces.Add(workforceDesc);
                }

                wk.UserNameSubmitted = req.UserName;
                wk.Timestamp = req.Now;

                wk.Validate();

                req.DB.Workforces.Upsert(wk)
                    .On(w => new { w.WorkforceId })
                    .Run();

                req.DB.WorkforceDescriptions.UpsertRange(wk.Workforces)
                    .On(wd => new { wd.WorkforceDescriptionId })
                    .Run();

                var needs = wk.Workforces.SelectMany(wd => wd.WorkforceNeeds);
                req.DB.WorkforceNeeds.UpsertRange(needs)
                    .On(n => new { n.WorkforceNeedId })
                    .Run();

                req.DB.SaveChanges();
                return HttpStatusCode.OK;
            }
        }

        private Response PostWorkforceUpdated()
        {
            using (var req = new FIORequest<JSONRepresentations.WorkforceWorkforcesUpdated.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var data = req.JsonPayload.payload;

                var wk = new Workforce();
                wk.WorkforceId = data.siteId;
                wk.SiteId = data.siteId;
                wk.PlanetId = data.address.lines[1].entity.id;
                wk.PlanetNaturalId = data.address.lines[1].entity.naturalId;
                wk.PlanetName = data.address.lines[1].entity.name;
                wk.LastWorkforceUpdateTime = req.Now;

                foreach (var workforce in data.workforces)
                {
                    WorkforceDescription workforceDesc = new WorkforceDescription();
                    workforceDesc.WorkforceDescriptionId = $"{wk.WorkforceId}-{workforce.level}";
                    workforceDesc.WorkforceTypeName = workforce.level;
                    workforceDesc.Population = workforce.population;
                    workforceDesc.Reserve = workforce.reserve;
                    workforceDesc.Capacity = workforce.capacity;
                    workforceDesc.Required = workforce.required;
                    workforceDesc.Satisfaction = workforce.satisfaction;
                    workforceDesc.WorkforceId = wk.WorkforceId;

                    foreach (var need in workforce.needs)
                    {
                        WorkforceNeed workforceNeed = new WorkforceNeed();
                        workforceNeed.WorkforceNeedId = $"{workforceDesc.WorkforceDescriptionId}-{need.material.ticker}";
                        workforceNeed.Category = need.category;
                        workforceNeed.Essential = need.essential;
                        workforceNeed.MaterialId = need.material.id;
                        workforceNeed.MaterialName = need.material.name;
                        workforceNeed.MaterialTicker = need.material.ticker;
                        workforceNeed.Satisfaction = need.satisfaction;
                        workforceNeed.UnitsPerInterval = need.unitsPerInterval;
                        workforceNeed.UnitsPerOneHundred = need.unitsPer100;
                        workforceNeed.WorkforceDescriptionId = workforceDesc.WorkforceDescriptionId;

                        workforceDesc.WorkforceNeeds.Add(workforceNeed);
                    }

                    wk.Workforces.Add(workforceDesc);
                }

                wk.UserNameSubmitted = req.UserName;
                wk.Timestamp = req.Now;

                wk.Validate();

                req.DB.Workforces.Upsert(wk)
                    .On(w => new { w.WorkforceId })
                    .Run();

                req.DB.WorkforceDescriptions.UpsertRange(wk.Workforces)
                    .On(wd => new { wd.WorkforceDescriptionId })
                    .Run();

                var needs = wk.Workforces.SelectMany(wd => wd.WorkforceNeeds);
                req.DB.WorkforceNeeds.UpsertRange(needs)
                    .On(n => new { n.WorkforceNeedId })
                    .Run();

                req.DB.SaveChanges();

                return HttpStatusCode.OK;
            }
        }

        private Response GetWorkforce(string UserName)
        {
            UserName = UserName.ToUpper();
            string RequesterUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Workforce))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var workforces = DB.Workforces
                        .AsNoTracking()
                        .Include(w => w.Workforces)
                            .ThenInclude(w => w.WorkforceNeeds)
                        .Where(s => s.UserNameSubmitted.ToUpper() == UserName)
                        .AsSplitQuery()
                        .ToList();
                    return JsonConvert.SerializeObject(workforces);
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        private Response GetWorkforce(string UserName, string Planet)
        {
            UserName = UserName.ToUpper();
            Planet = Planet.ToUpper();

            string RequesterUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Workforce))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var model = DB.Workforces
                        .AsNoTracking()
                        .Include(w => w.Workforces)
                            .ThenInclude(w => w.WorkforceNeeds)
                        .Where(s => s.UserNameSubmitted.ToUpper() == UserName && (s.PlanetId.ToUpper() == Planet || s.PlanetName.ToUpper() == Planet || s.PlanetNaturalId.ToUpper() == Planet))
                        .AsSplitQuery()
                        .FirstOrDefault();
                    if (model != null)
                    {
                        return JsonConvert.SerializeObject(model);
                    }
                    else
                    {
                        return HttpStatusCode.NoContent;
                    }
                }
            }

            return HttpStatusCode.Unauthorized;
        }

        private Response GetWorkforcePlanets(string UserName)
        {
            UserName = UserName.ToUpper();
            string RequesterUserName = Request.GetUserName();
            if (Auth.CanSeeData(RequesterUserName, UserName, Auth.PrivacyType.Workforce))
            {
                using (var DB = PRUNDataContext.GetNewContext())
                {
                    var workforcePlanets = DB.Workforces
                        .AsNoTracking()
                        .Where(s => s.UserNameSubmitted.ToUpper() == UserName)
                        .Select(s => s.PlanetNaturalId)
                        .ToList();
                    return JsonConvert.SerializeObject(workforcePlanets);
                }
            }

            return HttpStatusCode.Unauthorized;
        }
    }
}
#endif // WITH_MODULES