﻿using System;
using System.Collections.Generic;

namespace FIORest
{
    public class MRUCache<TKey, TValue> where TValue : class
    {
        private int capacity;

        private object lockObj = new object();
        private Dictionary<TKey, LinkedListNode<MRUCacheItem<TKey, TValue>>> cache = new Dictionary<TKey, LinkedListNode<MRUCacheItem<TKey, TValue>>>();
        private LinkedList<MRUCacheItem<TKey, TValue>> mruList = new LinkedList<MRUCacheItem<TKey, TValue>>();

        public MRUCache(int capacity)
        {
            if (capacity <= 0)
            {
                throw new ArgumentException("capacity must be larger than 0");
            }

            this.capacity = capacity;
        }

        public TValue Get(TKey key)
        {
            lock (lockObj)
            {
                if (cache.TryGetValue(key, out var node))
                {
                    TValue value = node.Value.value;
                    mruList.Remove(node);
                    mruList.AddLast(node);
                    return value;
                }
            }

            return default(TValue);
        }

        public void Set(TKey key, TValue value)
        {
            lock (lockObj)
            {
                if (cache.TryGetValue(key, out var item))
                {
                    mruList.Remove(item);
                }
                else if (cache.Count >= capacity)
                {
                    // Remove the first item from the LinkedList
                    var firstNode = mruList.First;
                    mruList.RemoveFirst();

                    cache.Remove(firstNode.Value.key);
                }

                var cacheItem = new MRUCacheItem<TKey, TValue>(key, value);
                var node = new LinkedListNode<MRUCacheItem<TKey, TValue>>(cacheItem);
                mruList.AddLast(node);
                cache[key] = node;
            }
        }

        public bool Remove(TKey key)
        {
            lock (lockObj)
            {
                if (cache.TryGetValue(key, out var item))
                {
                    mruList.Remove(item);
                    cache.Remove(item.Value.key);

                    return true;
                }
            }

            return false;
        }

        public void RemoveRange(IEnumerable<TKey> keys)
        {
            lock (lockObj)
            {
                foreach (var key in keys)
                {
                    Remove(key);
                }
            }
        }

        public void Clear()
        {
            lock (lockObj)
            {
                mruList.Clear();
                cache.Clear();
            }
        }
    }

    class MRUCacheItem<TKey, TValue> where TValue : class
    {
        public MRUCacheItem(TKey key, TValue value)
        {
            this.key = key;
            this.value = value;
        }

        public TKey key;
        public TValue value;
    }
}
