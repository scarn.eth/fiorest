﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

using Newtonsoft.Json;

using IndexAttribute = Microsoft.EntityFrameworkCore.IndexAttribute;

namespace FIORest.Database.Models
{
    [Index(nameof(UserName))]
    public class UserSettings
    {
        [Key]
        [JsonIgnore]
        [StringLength(32)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string UserSettingsId { get; set; } // UserName

        [StringLength(32)]
        public string UserName { get; set; }

        public virtual List<UserSettingsBurnRate> BurnRateSettings { get; set; } = new List<UserSettingsBurnRate>();

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(UserSettingsId) && UserSettingsId.Length <= 32 &&
                !String.IsNullOrEmpty(UserName) && UserName.Length <= 32
                );

            BurnRateSettings.ForEach(brs => brs.Validate());
        }
    }

    public class UserSettingsBurnRate
    {
        [Key]
        [JsonIgnore]
        [StringLength(41)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string UserSettingsBurnRateId { get; set; } // UserSettingsId-PlanetNaturalId

        [StringLength(8)]
        public string PlanetNaturalId { get; set; }

        public virtual List<UserSettingsBurnRateExclusion> MaterialExclusions { get; set; } = new List<UserSettingsBurnRateExclusion>();

        [Required]
        [JsonIgnore]
        [StringLength(32)]
        public string UserSettingsId { get; set; }

        [JsonIgnore]
        public virtual UserSettings UserSettings { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(UserSettingsBurnRateId) && UserSettingsBurnRateId.Length <= 41 &&
                !String.IsNullOrEmpty(PlanetNaturalId) && PlanetNaturalId.Length <= 8 &&
                !String.IsNullOrEmpty(UserSettingsId) && UserSettingsId.Length <= 32
                );

            MaterialExclusions.ForEach(me => me.Validate());
        }
    }

    public class UserSettingsBurnRateExclusion
    {
        [Key]
        [JsonIgnore]
        [StringLength(50)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string UserSettingsBurnRateExclusionId { get; set; } // UserSettingsBurnRateId-MaterialTicker

        [StringLength(8)]
        public string MaterialTicker { get; set; }

        [Required]
        [JsonIgnore]
        [StringLength(41)]
        public string UserSettingsBurnRateId { get; set; }

        [JsonIgnore]
        public virtual UserSettingsBurnRate UserSettingsBurnRate { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(UserSettingsBurnRateExclusionId) && UserSettingsBurnRateExclusionId.Length <= 50 &&
                !String.IsNullOrEmpty(MaterialTicker) && MaterialTicker.Length <= 8 &&
                !String.IsNullOrEmpty(UserSettingsBurnRateId) && UserSettingsBurnRateId.Length <= 41
                );
        }
    }
}
