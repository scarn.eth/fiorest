﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

using Newtonsoft.Json;

using IndexAttribute = Microsoft.EntityFrameworkCore.IndexAttribute;

namespace FIORest.Database.Models
{
    [Index(nameof(Name))]
    public class Sector
    {
        [Key]
        [JsonIgnore]
        [StringLength(32)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string SectorId { get; set; }

        [StringLength(32)]
        public string Name { get; set; }

        public int HexQ { get; set; }
        public int HexR { get; set; }
        public int HexS { get; set; }

        public int Size { get; set; }

        public virtual List<SubSector> SubSectors { get; set; } = new List<SubSector>();

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }

        public DateTime Timestamp { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(SectorId) && SectorId.Length <= 32 &&
                !String.IsNullOrEmpty(Name) && Name.Length <= 32 &&
                !String.IsNullOrEmpty(UserNameSubmitted) && UserNameSubmitted.Length <= 32 &&
                Timestamp != default
                );

            SubSectors.ForEach(ss => ss.Validate());
        }
    }

    public class SubSector
    {
        [Key]
        [JsonIgnore]
        [StringLength(32)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string SubSectorId { get; set; }

        public virtual List<SubSectorVertex> Vertices { get; set; } = new List<SubSectorVertex>();

        [Required]
        [JsonIgnore]
        [StringLength(32)]
        public string SectorId { get; set; }

        [JsonIgnore]
        public virtual Sector Sector { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(SubSectorId) && SubSectorId.Length <= 32 &&
                !String.IsNullOrEmpty(SectorId) && SectorId.Length <= 32
                );

            Vertices.ForEach(v => v.Validate());
        }
    }

    public class SubSectorVertex
    {
        [Key]
        [JsonIgnore]
        [StringLength(50)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string SubSectorVertexId { get; set; } // SubSectorId-VertexIndex (Make sure to order)

        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }

        [Required]
        [JsonIgnore]
        [StringLength(32)]
        public string SubSectorId { get; set; }

        [JsonIgnore]
        public virtual SubSector SubSector { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(SubSectorVertexId) && SubSectorVertexId.Length <= 50 &&
                !String.IsNullOrEmpty(SubSectorId) && SubSectorId.Length <= 32
                );
        }
    }
}
