﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class Experts
    {
        [Key]
        [JsonIgnore]
        [StringLength(32)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string ExpertsId { get; set; }

        [StringLength(32)]
        public string PlanetId { get; set; }

        public int AgricultureActive { get; set; }
        public int AgricultureAvailable { get; set; }
        public double AgricultureEfficiencyGain { get; set; }

        public int ResourceExtractioneActive { get; set; }
        public int ResourceExtractionAvailable { get; set; }
        public double ResourceExtractionEfficiencyGain { get; set; }

        public int FoodIndustriesActive { get; set; }
        public int FoodIndustriesAvailable { get; set; }
        public double FoodIndustriesEfficiencyGain { get; set; }

        public int ChemistryActive { get; set; }
        public int ChemistryAvailable { get; set; }
        public double ChemistryEfficiencyGain { get; set; }

        public int ConstructionActive { get; set; }
        public int ConstructionAvailable { get; set; }
        public double ConstructionEfficiencyGain { get; set; }

        public int ElectronicsActive { get; set; }
        public int ElectronicsAvailable { get; set; }
        public double ElectronicsEfficiencyGain { get; set; }

        public int FuelRefiningActive { get; set; }
        public int FuelRefiningAvailable { get; set; }
        public double FuelRefiningEfficiencyGain { get; set; }

        public int ManufacturingActive { get; set; }
        public int ManufacturingAvailable { get; set; }
        public double ManufacturingEfficiencyGain { get; set; }

        public int MetallurgyActive { get; set; }
        public int MetallurgyAvailable { get; set; }
        public double MetallurgyEfficiencyGain { get; set; }

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(ExpertsId) && ExpertsId.Length == 32 &&
                !String.IsNullOrEmpty(PlanetId) && PlanetId.Length == 32 &&
                ExpertsId == PlanetId &&
                !String.IsNullOrEmpty(UserNameSubmitted) && UserNameSubmitted.Length <= 32 &&
                Timestamp != default
                );
        }
    }
}
