﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class ComexExchange
    {
        [Key]
        [StringLength(32)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string ComexExchangeId { get; set; }

        [StringLength(64)]
        public string ExchangeName { get; set; }
        [StringLength(8)]
        public string ExchangeCode { get; set; }

        [StringLength(32)]
        public string ExchangeOperatorId { get; set; }
        [StringLength(8)]
        public string ExchangeOperatorCode { get; set; }
        [StringLength(64)]
        public string ExchangeOperatorName { get; set; }

        public int CurrencyNumericCode { get; set; }

        [StringLength(8)]
        public string CurrencyCode { get; set; }
        [StringLength(32)]
        public string CurrencyName { get; set; }
        public int CurrencyDecimals { get; set; }

        [StringLength(32)]
        public string LocationId { get; set; }
        [StringLength(64)]
        public string LocationName { get; set; }
        [StringLength(16)]
        public string LocationNaturalId { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(ComexExchangeId) && ComexExchangeId.Length == 32 &&
                !String.IsNullOrEmpty(ExchangeName) && ExchangeName.Length <= 64 &&
                !String.IsNullOrEmpty(ExchangeCode) && ExchangeCode.Length <= 8 &&
                (ExchangeOperatorId == null || ExchangeOperatorId.Length == 32 ) &&
                (ExchangeOperatorCode == null || ExchangeOperatorCode.Length <= 8) &&
                (ExchangeOperatorName == null || ExchangeOperatorName.Length <= 64) &&
                !String.IsNullOrEmpty(CurrencyCode) && CurrencyCode.Length <= 8 &&
                !String.IsNullOrEmpty(CurrencyName) && CurrencyName.Length <= 32 &&
                CurrencyDecimals > 0 &&
                !String.IsNullOrEmpty(LocationId) && LocationId.Length == 32 &&
                !String.IsNullOrEmpty(LocationName) && LocationName.Length <= 64 &&
                !String.IsNullOrEmpty(LocationNaturalId) && LocationNaturalId.Length <= 16
                );
        }
    }


    public class CountryRegistryCountry
    {
        [Key]
        [StringLength(32)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string CountryRegistryCountryId { get; set; }

        [StringLength(8)]
        public string CountryCode { get; set; }
        [StringLength(64)]
        public string CountryName { get; set; }

        public int CurrencyNumericCode { get; set; }

        [StringLength(8)]
        public string CurrencyCode { get; set; }
        [StringLength(32)]
        public string CurrencyName { get; set; }

        public int CurrencyDecimals { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(CountryRegistryCountryId) && CountryRegistryCountryId.Length == 32 &&
                !String.IsNullOrEmpty(CountryCode) && CountryCode.Length <= 8 &&
                !String.IsNullOrEmpty(CountryName) && CountryName.Length <= 64 &&
                !String.IsNullOrEmpty(CurrencyCode) && CurrencyCode.Length <= 8 &&
                !String.IsNullOrEmpty(CurrencyName) && CurrencyName.Length <= 32 &&
                CurrencyDecimals > 0
                );
        }
    }

    public class SimulationData
    {
        [JsonIgnore]
        public int SimulationDataId { get; set; }

        public int SimulationInterval { get; set; }
        public int FlightSTLFactor { get; set; }
        public int FlightFTLFactor { get; set; }
        public int PlanetaryMotionFactor { get; set; }
        public int ParsecLength { get; set; }

        public void Validate()
        {
            Debug.Assert(
                SimulationInterval > 0 &&
                FlightSTLFactor > 0 &&
                FlightFTLFactor > 0 &&
                PlanetaryMotionFactor > 0 &&
                ParsecLength > 0
                );
        }
    }

    public class WorkforcePerOneHundred
    {
        [JsonIgnore]
        public int WorkforcePerOneHundredId { get; set; }

        [StringLength(16)]
        public string WorkforceType { get; set; }

        public virtual List<WorkforcePerOneHundreedNeed> Needs { get; set; } = new List<WorkforcePerOneHundreedNeed>();

        public void Validate()
        {
            Debug.Assert(!String.IsNullOrEmpty(WorkforceType) && WorkforceType.Length <= 16);
            Needs.ForEach(n => n.Validate());
        }
    }

    public class WorkforcePerOneHundreedNeed
    {
        [JsonIgnore]
        public int WorkforcePerOneHundreedNeedId { get; set; }

        [StringLength(32)]
        public string MaterialId { get; set; }
        [StringLength(64)]
        public string MaterialName { get; set; }
        [StringLength(8)]
        public string MaterialTicker { get; set; }
        [StringLength(32)]
        public string MaterialCategory { get; set; }
        public double Amount { get; set; }

        [Required]
        [JsonIgnore]
        public int WorkforcePerOneHundredId { get; set; }

        [JsonIgnore]
        public virtual WorkforcePerOneHundred WorkforcePerOneHundred { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(MaterialId) && MaterialId.Length == 32 &&
                !String.IsNullOrEmpty(MaterialName) && MaterialName.Length <= 64 &&
                !String.IsNullOrEmpty(MaterialTicker) && MaterialTicker.Length <= 8 &&
                !String.IsNullOrEmpty(MaterialCategory) && MaterialCategory.Length <= 32 &&
                Amount > 0.0
                );
        }
    }
}
