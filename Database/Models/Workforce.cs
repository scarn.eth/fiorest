﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

using Newtonsoft.Json;

using IndexAttribute = Microsoft.EntityFrameworkCore.IndexAttribute;

namespace FIORest.Database.Models
{
    [Index(nameof(PlanetId))]
    [Index(nameof(PlanetNaturalId))]
    [Index(nameof(PlanetName))]
    [Index(nameof(SiteId))]
    public class Workforce
    {
        [Key]
        [JsonIgnore]
        [StringLength(32)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string WorkforceId { get; set; } // SiteId

        [StringLength(32)]
        public string PlanetId { get; set; }

        [StringLength(8)]
        public string PlanetNaturalId { get; set; }

        [StringLength(32)]
        public string PlanetName { get; set; }

        [StringLength(32)]
        public string SiteId { get; set; }

        public virtual List<WorkforceDescription> Workforces { get; set; } = new List<WorkforceDescription>();

        public DateTime? LastWorkforceUpdateTime { get; set; }

        [StringLength(32)]
        public string UserNameSubmitted { get; set; }

        public DateTime Timestamp { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(WorkforceId) && WorkforceId.Length == 32 &&
                !String.IsNullOrEmpty(PlanetId) && PlanetId.Length == 32 &&
                !String.IsNullOrEmpty(PlanetNaturalId) && PlanetNaturalId.Length <= 8 &&
                !String.IsNullOrEmpty(PlanetName) && PlanetName.Length <= 32 &&
                !String.IsNullOrEmpty(SiteId) && SiteId.Length == 32 &&
                !String.IsNullOrEmpty(UserNameSubmitted) && UserNameSubmitted.Length <= 32 &&
                Timestamp != default
                );

            Workforces.ForEach(w => w.Validate());
        }
    }

    public class WorkforceDescription
    {
        [Key]
        [JsonIgnore]
        [StringLength(50)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string WorkforceDescriptionId { get; set; } // WorkforceId-WorkforceTypeName

        [StringLength(16)]
        public string WorkforceTypeName { get; set; }

        public int Population { get; set; }
        public int Reserve { get; set; }
        public int Capacity { get; set; }
        public int Required { get; set; }

        public double Satisfaction { get; set; }

        public virtual List<WorkforceNeed> WorkforceNeeds { get; set; } = new List<WorkforceNeed>();

        [Required]
        [JsonIgnore]
        [StringLength(32)]
        public string WorkforceId { get; set; }

        [JsonIgnore]
        public virtual Workforce Workforce { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(WorkforceDescriptionId) && WorkforceDescriptionId.Length <= 50 &&
                !String.IsNullOrEmpty(WorkforceTypeName) && WorkforceTypeName.Length <= 16 &&
                !String.IsNullOrEmpty(WorkforceId) && WorkforceId.Length == 32
                );

            WorkforceNeeds.ForEach(wn => wn.Validate());
        }
    }

    public class WorkforceNeed
    {
        [Key]
        [JsonIgnore]
        [StringLength(60)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string WorkforceNeedId { get; set; }  // WorkforceDescriptionId-MaterialTicker

        [StringLength(32)]
        public string Category { get; set; }

        public bool Essential { get; set; }

        [StringLength(32)]
        public string MaterialId { get; set; }

        [StringLength(64)]
        public string MaterialName { get; set; }

        [StringLength(8)]
        public string MaterialTicker { get; set; }

        public double Satisfaction { get; set; }
        public double UnitsPerInterval { get; set; }
        public double UnitsPerOneHundred { get; set; }

        [Required]
        [JsonIgnore]
        [StringLength(50)]
        public string WorkforceDescriptionId { get; set; }

        [JsonIgnore]
        public virtual WorkforceDescription WorkforceDescription { get; set; }

        public void Validate()
        {
            Debug.Assert(
                !String.IsNullOrEmpty(WorkforceNeedId) && WorkforceNeedId.Length <= 60 &&
                !String.IsNullOrEmpty(Category) && Category.Length <= 32 &&
                !String.IsNullOrEmpty(MaterialId) && MaterialId.Length == 32 &&
                !String.IsNullOrEmpty(MaterialName) && MaterialName.Length <= 64 &&
                !String.IsNullOrEmpty(MaterialTicker) && MaterialTicker.Length <= 8 &&
                !String.IsNullOrEmpty(WorkforceDescriptionId) && WorkforceDescriptionId.Length <= 50
                );
        }
    }
}
