﻿namespace FIORest.JSONRepresentations.InfrastructureProject
{
    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string actionId { get; set; }
        public int status { get; set; }
        public Message message { get; set; }
    }

    public class Message
    {
        public string messageType { get; set; }
        public Payload1 payload { get; set; }
    }

    public class Payload1
    {
        public Body body { get; set; }
        public string[] path { get; set; }
    }

    public class Body
    {
        public string type { get; set; }
        public string projectIdentifier { get; set; }
        public int level { get; set; }
        public int activeLevel { get; set; }
        public int currentLevel { get; set; }
        public Upgradecost[] upgradeCosts { get; set; }
        public float upgradeStatus { get; set; }
        public Upkeep[] upkeeps { get; set; }
        public Contribution[] contributions { get; set; }
        public string id { get; set; }
    }

    public class Upgradecost
    {
        public Material material { get; set; }
        public int amount { get; set; }
        public int currentAmount { get; set; }
    }

    public class Material
    {
        public string name { get; set; }
        public string id { get; set; }
        public string ticker { get; set; }
        public string category { get; set; }
        public float weight { get; set; }
        public float volume { get; set; }
    }

    public class Upkeep
    {
        public int stored { get; set; }
        public int storeCapacity { get; set; }
        public int duration { get; set; }
        public Nexttick nextTick { get; set; }
        public Material1 material { get; set; }
        public int amount { get; set; }
        public int currentAmount { get; set; }
    }

    public class Nexttick
    {
        public long timestamp { get; set; }
    }

    public class Material1
    {
        public string name { get; set; }
        public string id { get; set; }
        public string ticker { get; set; }
        public string category { get; set; }
        public float weight { get; set; }
        public float volume { get; set; }
    }

    public class Contribution
    {
        public Material2[] materials { get; set; }
        public Time time { get; set; }
        public Contributor contributor { get; set; }
    }

    public class Time
    {
        public long timestamp { get; set; }
    }

    public class Contributor
    {
        public string id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Material2
    {
        public Material3 material { get; set; }
        public int amount { get; set; }
    }

    public class Material3
    {
        public string name { get; set; }
        public string id { get; set; }
        public string ticker { get; set; }
        public string category { get; set; }
        public float weight { get; set; }
        public float volume { get; set; }
    }

}
