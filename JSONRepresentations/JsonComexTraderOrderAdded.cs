﻿namespace FIORest.JSONRepresentations.JsonComexTraderOrderAdded
{

    public class Rootobject
    {
        public string messageType { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string id { get; set; }
        public Exchange exchange { get; set; }
        public string brokerId { get; set; }
        public string type { get; set; }
        public Material material { get; set; }
        public int amount { get; set; }
        public int initialAmount { get; set; }
        public Limit limit { get; set; }
        public string status { get; set; }
        public Created created { get; set; }
        public object[] trades { get; set; }
    }

    public class Exchange
    {
        public string id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string _type { get; set; }
        public string _proxy_key { get; set; }
    }

    public class Material
    {
        public string name { get; set; }
        public string id { get; set; }
        public string ticker { get; set; }
        public string category { get; set; }
        public float weight { get; set; }
        public float volume { get; set; }
    }

    public class Limit
    {
        public float amount { get; set; }
        public string currency { get; set; }
    }

    public class Created
    {
        public long timestamp { get; set; }
    }

}
